from PyQt5.QtWidgets import QMainWindow, QMdiArea, QApplication, \
    QMdiSubWindow, QTextEdit, QAction
from PythonComponent.shared.gRPC.dummyClient import DummyClient
from PythonComponent.shared.gRPC.grpcClient import GrpcClient
from PythonComponent.shared.gRPC.clientInterface import ClientInterface
import logging
from PyQt5 import QtCore
from PythonComponent.shared.utilities.printUtil import *
import time
from PythonComponent.pc.QtBasedGUI.motionControlSW import MotionControlSubWindow, create_motion_sub_window
from PythonComponent.pc.QtBasedGUI.SubWindowHandler import SubWindowHandler, ParentMainWindowInfo


class MdiMainWindow(QMainWindow):
    """
    This creates the main window with MDI.
    The class contains the communication client as a class attribute.
    Sub-windows access the communication client to make their own updates.
    Global updates are not used.
    Sub-windows are created via SubWindowHandler

    By default, the actual client is used, so to run this file, the server at:
    PythonComponent.shared.gRPC.test.grpcTestWithDummy.grpcServerWithDummy
    needs to be run.

    As the DummyClient has been fixed, current code uses DummyClient to make life easier
    """

    def __init__(self, comm_client=None):
        """
        Args:
            comm_client (ClientInterface): a running comm client. If None is given, a DummyClient will be used
        """
        super().__init__()
        # check what to use as client
        if comm_client is None:
            self.comm_client = DummyClient()
        else:
            self.comm_client = comm_client
        # check the client type
        if not isinstance(self.comm_client, ClientInterface):
            raise TypeError
        logging.info("communication client started running")

        # create essential components of the GUI
        self.main_menu = self.menuBar()
        self.view_menu = self.main_menu.addMenu("View")
        self.mdi_area = QMdiArea()
        self.setCentralWidget(self.mdi_area)
        self.setWindowTitle("MDI")
        self.show()

        sub = QMdiSubWindow()
        sub.setWidget(QTextEdit())
        sub.setWindowTitle("Quick Notes")
        self.mdi_area.addSubWindow(sub)
        sub.show()

        parent_info = ParentMainWindowInfo(self, self.mdi_area, self.view_menu)

        motion_control = SubWindowHandler(parent_info,
                                          lambda: MotionControlSubWindow(self.comm_client, 200),
                                          "Motion Control")


client = DummyClient()
logging.basicConfig(level=logging.DEBUG)
app = QApplication([])
window = MdiMainWindow()
app.exec_()
