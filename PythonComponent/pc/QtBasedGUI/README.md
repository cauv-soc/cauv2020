# GUI Structure
## Overview
The GUI uses Multiple Document Interface (see [link1](https://doc.qt.io/qt-5/qmdiarea.html), [link2](https://doc.qt.io/qt-5/qmdiarea.html)).
Each "function" of the GUI is packaged into a subwindow. The main window does not care about the subwindow.

## Components
### Main Window
*   Main window contains an instance of client.
*   The main window handles the dynamic creation and destruction of sub windows.

### Sub Windows
*   There is [QTimer](https://doc.qt.io/qt-5/qtimer.html) in each sub window, which triggers display update
*   The sub window should not connect its signal/slot to anything outside the sub window
*   The sub window performs update by calling the relevant functions in the client.

## Development
*   Make the .ui file for the sub window using Qt creator. Simple linking can be done in Qt Creator.
*   Add it to code_gen.py so that the python class will be generated.
*   Make a class that inherits QtWidgets.QMdiSubWindow and use the generated python class to populate the sub window.
*   Deal with any residual linking that needs to be handled manually within the sub window.
*   Add the sub window to the dynamic control in the main window (in the form of an action).
