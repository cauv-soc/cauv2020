from PyQt5 import QtCore
from PyQt5.QtWidgets import QAction, QMainWindow, QMdiArea, QMenu
import sys
import logging

class ParentMainWindowInfo(object):
    def __init__(self, parent_window, parent_mdi_area, parent_menu):
        """
        Args:
            parent_window (QMainWindow):
            parent_mdi_area (QMdiArea):
            parent_menu (QMenu):
        """
        # Types are checked
        if not isinstance(parent_window, QMainWindow):
            raise TypeError
        self.parent_window = parent_window

        if not isinstance(parent_mdi_area, QMdiArea):
            raise TypeError
        self.parent_mdi_area = parent_mdi_area

        if not isinstance(parent_menu, QMenu):
            raise TypeError
        self.parent_menu = parent_menu


class SubWindowHandler(object):
    def __init__(self, parent_window_info, sub_window_creation_func, action_name):
        super().__init__()
        if not isinstance(parent_window_info, ParentMainWindowInfo):
            raise TypeError
        parent_window = parent_window_info.parent_window
        parent_mdi_area = parent_window_info.parent_mdi_area
        parent_menu = parent_window_info.parent_menu

        self.window_alive = False
        self.sub_window = None
        self.parent_mdi_area = parent_mdi_area
        self.sub_window_creation_func = sub_window_creation_func
        # check that the input parameter is proper
        if not isinstance(action_name, str):
            print("Action name is not a string")
            sys.exit()
        self.create_window_action = QAction(action_name, parent=parent_window)
        self.create_window_action.triggered.connect(lambda: self.handle_action())
        parent_menu.addAction(self.create_window_action)
        self.create_window_action.setCheckable(True)

    def handle_action(self):
        logger = logging.getLogger(__name__)
        logger.debug("action triggered")
        if self.window_alive:
            logger.debug("Closing the window")
            self.sub_window.close()
            self.window_alive = False
        else:
            logger.debug("Opening the window")
            self.sub_window = self.sub_window_creation_func()
            self.window_alive = True

            logger.debug("window opened")
            self.parent_mdi_area.addSubWindow(self.sub_window)
            self.sub_window.show()
            self.sub_window.close_signal.connect(lambda: self.on_subwindow_close())
        logger.debug("Done handling action")

    def on_subwindow_close(self):
        self.window_alive = False
        self.create_window_action.setChecked(False)

    def turn_window_on(self):
        if self.window_alive:
            return
        else:
            self.create_window_action.trigger()
