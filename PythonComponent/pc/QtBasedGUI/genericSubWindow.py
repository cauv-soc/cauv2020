from PyQt5 import QtWidgets, QtCore
from PyQt5.QtCore import pyqtSignal


class GenericSubWindow(QtWidgets.QMdiSubWindow):
    close_signal = pyqtSignal()

    def __init__(self, update_freq=50):
        super().__init__()
        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.update_disp)
        self.timer.start(update_freq)

    def update_disp(self):
        pass

    def closeEvent(self, QCloseEvent):
        print("Oh no! The window is closed")
        self.timer.stop()
        super().closeEvent(QCloseEvent)
        self.close_signal.emit()