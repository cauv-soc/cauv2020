from PythonComponent.pc.QtBasedGUI.genericSubWindow import GenericSubWindow
from PythonComponent.pc.QtBasedGUI.motionControl import Ui_MotionControl
from PyQt5 import QtWidgets
from PythonComponent.shared.gRPC.containers import *


class MotionControlSubWindow(GenericSubWindow):
    def __init__(self, client=None, update_freq=50):
        super().__init__(update_freq)
        self.client = client
        self.motion_control = QtWidgets.QWidget()
        self.ui = Ui_MotionControl()
        self.ui.setupUi(self.motion_control)
        self.setWidget(self.motion_control)
        # self.timer = QtCore.QTimer(self)
        # self.timer.timeout.connect(self.update_disp)
        # self.timer.start(update_freq)

    def update_disp(self):
        # logger = logging.getLogger(__name__)
        # logger.debug("running update")
        if self.client is None:
            # logger.debug("Client not initiated. No update is done")
            return
        current_imu = self.client.read_imu()
        curr_rotation = current_imu.rotation
        # do some checking to make sure that we can getting the correct thing
        if isinstance(curr_rotation, Rotation):
            yaw = int(curr_rotation.yaw)
            pitch = int(curr_rotation.pitch)
            roll = int(curr_rotation.roll)
            self.ui.c_yaw_lcd.display(yaw)
            self.ui.c_pitch_lcd.display(pitch)
            self.ui.c_roll_lcd.display(roll)

    # def closeEvent(self, QCloseEvent):
    #     print("Oh no! The window is closed")
    #     self.timer.stop()
    #     super().closeEvent(QCloseEvent)


def create_motion_sub_window(client=None, update_freq=50):
    return MotionControlSubWindow(client, update_freq)
