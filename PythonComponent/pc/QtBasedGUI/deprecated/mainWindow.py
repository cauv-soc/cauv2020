# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_GUI_file.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtMultimediaWidgets import QVideoWidget
from PythonComponent.shared.gRPC.dummyClient import DummyClient

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1106, 985)

        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.dataLayout = QtWidgets.QGridLayout()
        self.dataLayout.setObjectName("dataLayout")
        self.depthOutput = QtWidgets.QLCDNumber(self.centralwidget)
        self.depthOutput.setObjectName("depthOutput")
        self.dataLayout.addWidget(self.depthOutput, 1, 0, 1, 1)
        self.depthLabel = QtWidgets.QLabel(self.centralwidget)
        self.depthLabel.setObjectName("depthLabel")
        self.dataLayout.addWidget(self.depthLabel, 1, 1, 1, 1)
        self.sonarLabel = QtWidgets.QLabel(self.centralwidget)
        self.sonarLabel.setObjectName("sonarLabel")
        self.dataLayout.addWidget(self.sonarLabel, 2, 1, 1, 1)
        self.batteryLabel = QtWidgets.QLabel(self.centralwidget)
        self.batteryLabel.setObjectName("batteryLabel")
        self.dataLayout.addWidget(self.batteryLabel, 0, 1, 1, 1)
        self.batteryBar = QtWidgets.QProgressBar(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.batteryBar.sizePolicy().hasHeightForWidth())
        self.batteryBar.setSizePolicy(sizePolicy)
        self.batteryBar.setProperty("value", 24)
        self.batteryBar.setObjectName("batteryBar")
        self.dataLayout.addWidget(self.batteryBar, 0, 0, 1, 1)

        # Sonar
        self.sonarOutput = QtWidgets.QLCDNumber(self.centralwidget)
        self.sonarOutput.setObjectName("sonarOutput")
        self.dataLayout.addWidget(self.sonarOutput, 2, 0, 1, 1)

        self.gridLayout_3.addLayout(self.dataLayout, 1, 1, 1, 1)
        self.videoStream = QVideoWidget(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.videoStream.sizePolicy().hasHeightForWidth())
        self.videoStream.setSizePolicy(sizePolicy)
        self.videoStream.setMinimumSize(QtCore.QSize(500, 500))
        self.videoStream.setMaximumSize(QtCore.QSize(800, 800))
        self.videoStream.setObjectName("videoStream")
        self.gridLayout_3.addWidget(self.videoStream, 0, 0, 2, 1)
        self.miscBarLayout = QtWidgets.QFormLayout()
        self.miscBarLayout.setObjectName("miscBarLayout")
        self.lightswitchButton = QtWidgets.QRadioButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lightswitchButton.sizePolicy().hasHeightForWidth())
        self.lightswitchButton.setSizePolicy(sizePolicy)
        self.lightswitchButton.setObjectName("lightswitchButton")
        self.miscBarLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.lightswitchButton)
        self.resetButton = QtWidgets.QPushButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.resetButton.sizePolicy().hasHeightForWidth())
        self.resetButton.setSizePolicy(sizePolicy)
        self.resetButton.setObjectName("resetButton")
        self.miscBarLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.resetButton)
        self.verticalSlider = QtWidgets.QSlider(self.centralwidget)
        self.verticalSlider.setOrientation(QtCore.Qt.Vertical)
        self.verticalSlider.setObjectName("verticalSlider")
        self.miscBarLayout.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.verticalSlider)
        self.gridLayout_3.addLayout(self.miscBarLayout, 3, 0, 1, 1)
        self.setValueLayout = QtWidgets.QGridLayout()
        self.setValueLayout.setObjectName("setValueLayout")
        self.pitchOutput = QtWidgets.QLCDNumber(self.centralwidget)
        self.pitchOutput.setObjectName("pitchOutput")
        self.setValueLayout.addWidget(self.pitchOutput, 2, 2, 1, 1)


        # The following four blocks are the horizontal set value sliders, the signal/slots are at the end of this group
        self.velocitySlider = QtWidgets.QSlider(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.velocitySlider.sizePolicy().hasHeightForWidth())
        self.velocitySlider.setSizePolicy(sizePolicy)
        self.velocitySlider.setOrientation(QtCore.Qt.Horizontal)
        self.velocitySlider.setObjectName("velocitySlider")
        self.setValueLayout.addWidget(self.velocitySlider, 1, 1, 1, 1)

        self.pitchSlider = QtWidgets.QSlider(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pitchSlider.sizePolicy().hasHeightForWidth())
        self.pitchSlider.setSizePolicy(sizePolicy)
        self.pitchSlider.setOrientation(QtCore.Qt.Horizontal)
        self.pitchSlider.setObjectName("pitchSlider")
        self.setValueLayout.addWidget(self.pitchSlider, 2, 1, 1, 1)

        self.rollSlider = QtWidgets.QSlider(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.rollSlider.sizePolicy().hasHeightForWidth())
        self.rollSlider.setSizePolicy(sizePolicy)
        self.rollSlider.setOrientation(QtCore.Qt.Horizontal)
        self.rollSlider.setObjectName("rollSlider")
        self.setValueLayout.addWidget(self.rollSlider, 5, 1, 1, 1)

        self.yawSlider = QtWidgets.QSlider(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.yawSlider.sizePolicy().hasHeightForWidth())
        self.yawSlider.setSizePolicy(sizePolicy)
        self.yawSlider.setOrientation(QtCore.Qt.Horizontal)
        self.yawSlider.setObjectName("yawSlider")
        self.setValueLayout.addWidget(self.yawSlider, 3, 1, 1, 1)

        self.velocitySlider.sliderReleased.connect(self.set_velocity)

        self.velocityOutput = QtWidgets.QLCDNumber(self.centralwidget)
        self.velocityOutput.setObjectName("velocityOutput")
        self.setValueLayout.addWidget(self.velocityOutput, 1, 2, 1, 1)
        self.pitchLabel = QtWidgets.QLabel(self.centralwidget)
        self.pitchLabel.setObjectName("pitchLabel")
        self.setValueLayout.addWidget(self.pitchLabel, 2, 0, 1, 1)
        self.velocityLabel = QtWidgets.QLabel(self.centralwidget)
        self.velocityLabel.setObjectName("velocityLabel")
        self.setValueLayout.addWidget(self.velocityLabel, 1, 0, 1, 1)
        self.currentValueLabel = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.currentValueLabel.sizePolicy().hasHeightForWidth())
        self.currentValueLabel.setSizePolicy(sizePolicy)
        self.currentValueLabel.setObjectName("currentValueLabel")
        self.setValueLayout.addWidget(self.currentValueLabel, 0, 2, 1, 1)
        self.yawLabel = QtWidgets.QLabel(self.centralwidget)
        self.yawLabel.setObjectName("yawLabel")
        self.setValueLayout.addWidget(self.yawLabel, 3, 0, 1, 1)
        self.yawOutput = QtWidgets.QLCDNumber(self.centralwidget)
        self.yawOutput.setObjectName("yawOutput")
        self.setValueLayout.addWidget(self.yawOutput, 3, 2, 1, 1)
        self.rollLabel = QtWidgets.QLabel(self.centralwidget)
        self.rollLabel.setObjectName("rollLabel")
        self.setValueLayout.addWidget(self.rollLabel, 5, 0, 1, 1)



        self.rollOutput = QtWidgets.QLCDNumber(self.centralwidget)
        self.rollOutput.setObjectName("rollOutput")
        self.setValueLayout.addWidget(self.rollOutput, 5, 2, 1, 1)

        self.setValueLabel = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.setValueLabel.sizePolicy().hasHeightForWidth())
        self.setValueLabel.setSizePolicy(sizePolicy)
        self.setValueLabel.setObjectName("setValueLabel")
        self.setValueLayout.addWidget(self.setValueLabel, 0, 1, 1, 1)
        self.gridLayout_3.addLayout(self.setValueLayout, 0, 1, 1, 1)
        self.orientationStream = QtWidgets.QOpenGLWidget(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.orientationStream.sizePolicy().hasHeightForWidth())
        self.orientationStream.setSizePolicy(sizePolicy)
        self.orientationStream.setMinimumSize(QtCore.QSize(0, 200))
        self.orientationStream.setMaximumSize(QtCore.QSize(300, 300))
        self.orientationStream.setObjectName("orientationStream")
        self.gridLayout_3.addWidget(self.orientationStream, 5, 1, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1106, 28))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.toolBar = QtWidgets.QToolBar(MainWindow)
        self.toolBar.setObjectName("toolBar")
        MainWindow.addToolBar(QtCore.Qt.TopToolBarArea, self.toolBar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        self.updateValueTimer = QtCore.QTimer(MainWindow)
        self.updateValueTimer.setInterval(1000) #1 second
        self.updateValueTimer.timeout.connect(self.update_values)


    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.depthLabel.setText(_translate("MainWindow", "Depth"))
        self.sonarLabel.setText(_translate("MainWindow", "Sonar distance"))
        self.batteryLabel.setText(_translate("MainWindow", "Battery/time left"))
        self.lightswitchButton.setText(_translate("MainWindow", "Lights"))
        self.resetButton.setText(_translate("MainWindow", "Reset"))
        self.pitchLabel.setText(_translate("MainWindow", "Pitch"))
        self.velocityLabel.setText(_translate("MainWindow", "Velocity"))
        self.currentValueLabel.setText(_translate("MainWindow", "Current value"))
        self.yawLabel.setText(_translate("MainWindow", "Yaw"))
        self.rollLabel.setText(_translate("MainWindow", "Roll"))
        self.setValueLabel.setText(_translate("MainWindow", "Set value"))
        self.toolBar.setWindowTitle(_translate("MainWindow", "toolBar"))

    def update_values(self):
        dummyClientInstance = DummyClient()

        #Update sonar
        self.sonarOutput.display(dummyClientInstance.read_sonar())

        #Update velocity - NOTE: This currently only displays the Target Velocity
        self.velocityOutput.display(dummyClientInstance.read_target_velocity())

        #Update pitch - NOTE: These three don't exist yet
        self.pitchOutput.display(dummyClientInstance.read_pitch())

        #Update roll
        self.rollOutput.display(dummyClientInstance.read_roll())

        #Update yaw
        self.yawOutput.display(dummyClientInstance.read_sonar())

        #Update battery percentage - NOTE: May need to convert to a percentage value first
        self.batteryBar.setValue(dummyClientInstance.read_battery_status())

    def set_velocity(self):
        # This only sets upon release of the slider.
        dummyClientInstance = DummyClient()
        value = self.velocitySlider.value()
        dummyClientInstance.set_velocity(value)

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
