def str_class(class_instance):
    """ Convert a python object to a proper string

    Args:
        class_instance (object): A Python object to be converted to string

    Returns:
        A string
    """
    overall_str = type(class_instance).__name__ + '\r\n'
    overall_str = overall_str + str(class_instance.__dict__) + '\r\n'
    return overall_str


def print_class(class_instance):
    """ Print a python object using str_class

    Args:
        class_instance (object): A Python object to be printed
    """
    print(str_class(class_instance), end='')


def str_grpc_class(class_instance, hide_unavailable_data=False, unavailable_thresh=0):
    """Convert grpc class to string

    Args:
        class_instance (GeneratedProtocolMessageType): The grpc object to be printed
        hide_unavailable_data (bool): Set if unavailable fields will be printed
        unavailable_thresh (int): Set the time_stamp threshold below which the field is considered unavailable

    Returns:
        A string
    """
    exclusion = ['ByteSize', 'Clear', 'ClearExtension', 'ClearField', 'CopyFrom', 'DESCRIPTOR',
                 'DiscardUnknownFields', 'FindInitializationErrors', 'FromString', 'HasExtension', 'HasField',
                 'IsInitialized', 'ListFields', 'MergeFrom', 'MergeFromString', 'ParseFromString',
                 'RegisterExtension', 'SerializePartialToString', 'SerializeToString', 'SetInParent',
                 'UnknownFields', 'WhichOneof', '_InternalParse', '_InternalSerialize', '_Modified', '_SetListener',
                 '_UpdateOneofState', '__class__', '__deepcopy__', '__delattr__', '__dir__', '__doc__', '__eq__',
                 '__format__', '__ge__', '__getattribute__', '__getstate__', '__gt__', '__hash__', '__init__',
                 '__init_subclass__', '__le__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__',
                 '__reduce_ex__', '__repr__', '__setattr__', '__setstate__', '__sizeof__', '__slots__', '__str__',
                 '__subclasshook__', '__unicode__', '__weakref__', '_cached_byte_size', '_cached_byte_size_dirty',
                 '_decoders_by_tag', '_extensions_by_name', '_extensions_by_number', '_fields',
                 '_is_present_in_parent', '_listener', '_listener_for_children', '_oneofs', '_unknown_field_set',
                 '_unknown_fields']
    overall_str = type(class_instance).__name__ + '\r\n'
    overall_str = overall_str + '-------------------------\r\n'
    for field in dir(class_instance):
        field_object = getattr(class_instance, field)
        if not hasattr(field_object, 'time_stamp'):
            continue
        field_str = field + ': '
        time_stamp = getattr(field_object, 'time_stamp')
        if time_stamp <= unavailable_thresh:
            if hide_unavailable_data:
                continue
            else:
                field_str = field_str + 'Data not available\r\n'
        else:
            field_str = field_str + '\r\n'
            for item in dir(field_object):
                if not (item in exclusion) and not ('FIELD_NUMBER' in item):
                    val = getattr(field_object, item)
                    field_str = field_str + item + ' = ' + str(val) + ', '
        overall_str = overall_str + field_str + '\r\n'
    overall_str = overall_str + '==============================\r\n'
    return overall_str


def print_grpc_class(class_instance, hide_unavailable_data=False, unavailable_thresh=0):
    """ Prints grpc class with str_grpc_class

    Args:
        class_instance (GeneratedProtocolMessageType): The grpc object to be printed
        hide_unavailable_data (bool): Set if unavailable fields will be printed
        unavailable_thresh (int): Set the time_stamp threshold below which the field is considered unavailable
    """
    print(str_grpc_class(class_instance, hide_unavailable_data, unavailable_thresh), end='')


# vel = Velocity()
# sensor_data_pack = SensorDataPack()
# print_class(vel)
# print(str_grpc_class(sensor_data_pack))
