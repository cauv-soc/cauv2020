import queue
import sys
import time


def read_latest(targetQueue):
    if not isinstance(targetQueue, queue.Queue):
        print('readLatest::ERROR, not a queue')
        sys.exit()
    lastItem = None
    while True:
        try:
            lastItem = targetQueue.get_nowait()
        except queue.Empty:
            break
    return lastItem


def read_latest_blocking(targetQueue):
    result = None
    while result is None:
        result = read_latest(targetQueue)
        if result is None:
            time.sleep(0.001)  # if there is nothing, wait for a while and try again
    return result
