import logging
import math
import time
import sys
from random import randint

from PythonComponent.shared.gRPC.clientInterface import ClientInterface
from PythonComponent.shared.gRPC.containers import *
from PythonComponent.shared.utilities import timeStamp


class DummyClient(ClientInterface):

    def __init__(self):
        self.log = logging.getLogger(__name__)

        # latest values of the data are stored here
        # In the real client these are obtained from the Jetson
        self.imu_data = IMUData()
        self.imu_data.rotation = CurrentRotation()
        self.imu_data.acceleration = CurrentAcceleration()
        self.imu_data.angular_velocity = CurrentAngularVelocity()

        self.sonar_data = SonarData()
        self.depth_data = DepthData()
        self.img_data = ImgData()

        self.NUM_THRUSTER = 6
        self.thrusters_status = ThrusterStatus(speed=[0] * self.NUM_THRUSTER)

        self.NUM_BATTERIES = 3
        self.battery_status = BatteryStatus(voltage=[0.0] * self.NUM_BATTERIES,
                                            percentage=[0.0] * self.NUM_BATTERIES)

        # sweep variables
        self.sweep = True
        self.sweep_start_t = 0

        self.THRUSTER_RANGE = (0, 100)
        self.THRUSTER_SWEEP_SPEED = 0.2

        self.ANGLE_RANGE = (-180.0, 180.0)
        self.PITCH_RANGE = (-90.0, 90.0)
        self.ANGLE_SWEEP_SPEED = 0.2

        self.SONAR_RANGE = (0.0, 100.0)
        self.SONAR_SWEEP_SPEED = 0.2

        self.DEPTH_RANGE = (0.0, 50.0)
        self.DEPTH_SWEEP_SPEED = 0.2

        self.VOLTAGE_RANGE = (14.0, 16.8)
        self.VOLTAGE_SWEEP_SPEED = 0.2

        self.LIGHT_RANGE = (0,100)
        self.LIGHT_SWEEP_SPEED = 0.2

        # variables set by the ground station
        self.target_motion = TargetMotion()
        self.target_motion.rotation = TargetRotation()
        self.target_motion.velocity = TargetVelocity()

        self.NUM_LIGHTS = 4
        self.lights = LightBrightness(index=[i for i in range(self.NUM_LIGHTS)],
                                      brightness=[0] * self.NUM_LIGHTS)

        self.set_example_data()

    # for the functions to make RPC calls and
    # responses will probably only be made accessible to the GUI/ controller if an error has occured
    def set_velocity(self, velocity):
        if isinstance(velocity, TargetVelocity):
            self.target_motion.velocity = velocity
            return ErrorStatus.NO_ERROR
        else:
            return ErrorStatus.WRONG_DATA_TYPE

    def set_rotation(self, rotation):
        if isinstance(rotation, TargetRotation):
            self.target_motion.velocity = rotation
            return ErrorStatus.NO_ERROR
        else:
            return ErrorStatus.WRONG_DATA_TYPE

    def set_light_brightness(self, light_index, brightness):
        self.lights.brightness[light_index] = brightness
        return ErrorStatus.NO_ERROR

    def set_pid_value(self, pid_value):
        return ErrorStatus.NO_ERROR

    # give the GUI the most recent data collected from
    # the Jetson, from the relevant class variables
    # If data is being updated these functions block,
    # but should not do so for any extended periods of time
    # when actually making RPC calls, we may want non blocking functionality
    def read_target_velocity(self):
        return self.target_motion.velocity

    def read_target_rotation(self):
        return self.target_motion.rotation

    def read_light_brightness(self):
        return self.lights

    def read_imu(self):
        if self.sweep:
            self.test_sweep_parameters()
        return self.imu_data

    def read_thruster_status(self):
        if self.sweep:
            self.test_sweep_parameters()
        return self.thrusters_status

    def read_button_status(self):
        return ErrorStatus.NO_IMPLEMENTATION
        # raise NotImplementedError

    def read_depth(self):
        if self.sweep:
            self.test_sweep_parameters()
        return self.depth_data

    def read_sonar(self):
        if self.sweep:
            self.test_sweep_parameters()
        return self.sonar_data

    def read_img(self):
        if self.sweep:
            self.test_sweep_parameters()
        return self.img_data

    def read_battery_status(self):
        if self.sweep:
            self.test_sweep_parameters()
        return self.battery_status

    def emergency_stop(self):
        return ErrorStatus.PERMISSION_DENIED
        # return ErrorStatus.NO_ERROR

    def hardware_reset(self):
        return ErrorStatus.PERMISSION_DENIED
        # return ErrorStatus.NO_ERROR

    def set_example_data(self):
        # sets some random plausible data for the system,
        # useful for debugging individual GUI issues and ensuring
        # all data is obtained correctly
        current_timestamp = timeStamp.curr_time()

        self.thrusters_status = ThrusterStatus(speed=[20, 53, 34, 67, 81, 14])
        self.thrusters_status.time_stamp = current_timestamp

        self.imu_data.rotation.roll = 35.0
        self.imu_data.rotation.pitch = -20.0
        self.imu_data.rotation.yaw = 146.0
        self.imu_data.time_stamp = current_timestamp
        self.imu_data.rotation.time_stamp = current_timestamp

        self.sonar_data.dist = 28.0
        self.sonar_data.time_stamp = current_timestamp
        self.depth_data.depth = 5.8
        self.depth_data.time_stamp = current_timestamp

        self.battery_status.voltage = [randint(140, 168)/10.0 for _ in range(self.NUM_BATTERIES)]
        self.battery_status.percentage = [randint(0, 100) for _ in range(self.NUM_BATTERIES)]
        self.battery_status.time_stamp = current_timestamp

        self.lights.brightness = [randint(0, 100) for _ in range(self.NUM_LIGHTS)]

    def start_sweep(self, thrusters=True, orientation=True, sonar=True, pressure=True):
        self.sweep_start_t = time.time()
        self.sweep = True

    def end_sweep(self):
        self.sweep = False

    def sweep_function(self, range, omega, phase=0.0):
        """sweeps values based on the sin function"""
        t = self.sweep_start_t - time.time()
        val = ((range[1] - range[0]) * math.sin(t * omega + phase) + (range[1] + range[0]))/2
        return val

    def test_sweep_parameters(self, thrusters=True, orientation=True, sonar=True, pressure=True, battery=True, lights=True):
        if thrusters:
            for i in range(self.NUM_THRUSTER):
                self.thrusters_status.speed[i] = self.sweep_function(self.THRUSTER_RANGE,
                                                                     self.THRUSTER_SWEEP_SPEED,
                                                                     2*math.pi*i/self.NUM_THRUSTER)
            self.thrusters_status.time_stamp = timeStamp.curr_time()

        if orientation:
            self.imu_data.rotation.roll = self.sweep_function(self.ANGLE_RANGE, self.ANGLE_SWEEP_SPEED, 0)
            self.imu_data.rotation.pitch = self.sweep_function(self.PITCH_RANGE, self.ANGLE_SWEEP_SPEED, 2*math.pi/3)
            self.imu_data.rotation.yaw = self.sweep_function(self.ANGLE_RANGE, self.ANGLE_SWEEP_SPEED, 4*math.pi/3)
            self.imu_data.rotation.time_stamp = timeStamp.curr_time()
            self.imu_data.time_stamp = timeStamp.curr_time()

        if sonar:
            self.sonar_data.dist = self.sweep_function(self.SONAR_RANGE, self.SONAR_SWEEP_SPEED)
            self.imu_data.time_stamp = timeStamp.curr_time()

        if pressure:
            self.depth_data.depth = self.sweep_function(self.DEPTH_RANGE, self.DEPTH_SWEEP_SPEED)
            self.depth_data.time_stamp = timeStamp.curr_time()

        if battery:
            v_sweep = round(self.sweep_function(self.VOLTAGE_RANGE, self.VOLTAGE_SWEEP_SPEED), 1)
            v_percentage = round((v_sweep-self.VOLTAGE_RANGE[0]) * 100/(self.VOLTAGE_RANGE[1] - self.VOLTAGE_RANGE[0]))

            self.battery_status.voltage = [v_sweep] * self.NUM_BATTERIES
            self.battery_status.percentage = [v_percentage] * self.NUM_BATTERIES
            self.battery_status.time_stamp = timeStamp.curr_time()

        if lights:
            light_sweep = round(self.sweep_function(self.LIGHT_RANGE, self.LIGHT_SWEEP_SPEED))

            self.lights.brightness = [light_sweep] * self.NUM_LIGHTS
            self.battery_status.time_stamp = timeStamp.curr_time()

    @staticmethod
    def constrain_val(val, val_range):
        # constrains an output to within a given range
        return max(min(val, val_range[1]), val_range[0])
