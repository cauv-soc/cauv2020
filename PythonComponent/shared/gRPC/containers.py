from enum import Enum, IntEnum


# if the names are very long, it will take up more space...
# but most of the space will be taken up by the image anyway, do don't bother...
# note that a raw img frame 640*480 ~ 92 k before encoding
# not sure if it is worth the effort to encode the image because that takes time
class StatusReportContainer(object):
    def __init__(self):
        self.imgFrameRaw = None
        self.thrusterSpeed = None


class GSInstructionContainer(object):
    def __init__(self):
        pass


class ImgContainer(object):
    def __init__(self):
        self.rawImg = None


# ignore everything above
class GenericData(object):
    def __init__(self, time_stamp=None):
        self.time_stamp = time_stamp
        self.msg = None

    def __repr__(self):
        p = "timestamp : " + str(self.time_stamp) + "\n"
        if self.msg:
            p += "message : " + self.msg + "\n"
        return p


class Rotation(GenericData):
    def __init__(self, yaw=None, pitch=None, roll=None):
        super().__init__()
        self.yaw = yaw
        self.pitch = pitch
        self.roll = roll

    def __getitem__(self, key):
        if key == 0:
            return self.yaw
        if key == 1:
            return self.pitch
        if key == 2:
            return self.roll

    def __setitem__(self, key, value):
        if key == 0:
            self.yaw = value
        if key == 1:
            self.pitch = value
        if key == 2:
            self.roll = value

    def __repr__(self):
        p = super().__repr__()
        p += "Roll : {}, Pitch : {}, Yaw : {}\n".format(self.roll, self.pitch, self.yaw)
        return p


class CurrentRotation(Rotation):
    def __init__(self, yaw=None, pitch=None, roll=None):
        super().__init__(yaw, pitch, roll)

    def __repr__(self):
        p = "CurrentRotation\n"
        p += super().__repr__()
        return p


class TargetRotation(Rotation):
    def __init__(self, yaw=None, pitch=None, roll=None):
        super().__init__(yaw, pitch, roll)

    def __repr__(self):
        p = "TargetRotation\n"
        p += super().__repr__()
        return p


class Velocity(GenericData):
    def __init__(self, vx=None, vy=None, vz=None):
        super().__init__()
        self.vx = vx
        self.vy = vy
        self.vz = vz

    def __getitem__(self, key):
        if key == 0:
            return self.vx
        if key == 1:
            return self.vy
        if key == 2:
            return self.vz

    def __setitem__(self, key, value):
        if key == 0:
            self.vx = value
        if key == 1:
            self.vy = value
        if key == 2:
            self.vz = value

    def __repr__(self):
        p = super().__repr__()
        p += "Velocity(x : {}, y : {}, z : {})\n".format(self.vx, self.vy, self.vz)
        return p


class CurrentVelocity(Velocity):
    def __init__(self, vx=None, vy=None, vz=None):
        super().__init__(vx, vy, vz)

    def __repr__(self):
        p = "CurrentVelocity\n"
        p += super().__repr__()
        return p


class TargetVelocity(Velocity):
    def __init__(self, vx=None, vy=None, vz=None):
        super().__init__(vx, vy, vz)

    def __repr__(self):
        p = "TargetVelocity\n"
        p += super().__repr__()
        return p


class Acceleration(GenericData):
    def __init__(self, ax=None, ay=None, az=None):
        super().__init__()
        self.ax = ax
        self.ay = ay
        self.az = az

    def __getitem__(self, key):
        if key == 0:
            return self.ax
        if key == 1:
            return self.ay
        if key == 2:
            return self.az

    def __setitem__(self, key, value):
        if key == 0:
            self.ax = value
        if key == 1:
            self.ay = value
        if key == 2:
            self.az = value

    def __repr__(self):
        p = super().__repr__()
        p += "Acceleration(x : {}, y : {}, z : {})\n".format(self.ax, self.ay, self.az)
        return p


class CurrentAcceleration(Acceleration):
    def __init__(self, ax=None, ay=None, az=None):
        super().__init__(ax, ay, az)

    def __repr__(self):
        p = "CurrentAcceleration\n"
        p += super().__repr__()
        return p


class TargetAcceleration(Acceleration):
    def __init__(self, ax=None, ay=None, az=None):
        super().__init__(ax, ay, az)

    def __repr__(self):
        p = "TargetAcceleration\n"
        p += super().__repr__()
        return p


class AngularVelocity(GenericData):
    def __init__(self, omega_x=None, omega_y=None, omega_z=None):
        super().__init__()
        self.omega_x = omega_x
        self.omega_y = omega_y
        self.omega_z = omega_z

    def __getitem__(self, key):
        if key == 0:
            return self.omega_x
        if key == 1:
            return self.omega_y
        if key == 2:
            return self.omega_z

    def __setitem__(self, key, value):
        if key == 0:
            self.omega_x = value
        if key == 1:
            self.omega_y = value
        if key == 2:
            self.omega_z = value

    def __repr__(self):
        p = super().__repr__()
        p += "Angular Velocity(x : {}, y : {}, z : {})\n".format(self.omega_x, self.omega_y, self.omega_z)
        return p


class CurrentAngularVelocity(AngularVelocity):
    def __init__(self, omega_x=None, omega_y=None, omega_z=None):
        super().__init__(omega_x, omega_y, omega_z)

    def __repr__(self):
        p = "CurrentAngularVelocity\n"
        p += super().__repr__()
        return p


class TargetAngularVelocity(AngularVelocity):
    def __init__(self, omega_x=None, omega_y=None, omega_z=None):
        super().__init__(omega_x, omega_y, omega_z)

    def __repr__(self):
        p = "TargetAngularVelocity\n"
        p += super().__repr__()
        return p


class LightBrightness(GenericData):
    def __init__(self, index=None, brightness=None):
        super().__init__()
        self.index = index
        self.brightness = brightness

    def __repr__(self):
        p = "LightBrightness\n"
        p += super().__repr__()
        p += "Light brightnesses : {}".format(self.brightness)
        return p


class TargetLightBrightness(LightBrightness):
    def __init__(self, index=None, brightness=None):
        super().__init__(index, brightness)

    def __repr__(self):
        p = "TargetLightBrightness\n"
        p += super().__repr__()


class LightInd(IntEnum):
    MBED_LED_1 = 4


class ButtonStatus(GenericData):
    def __init__(self):
        super().__init__()
        self.is_pressed = None

    def __repr__(self):
        p = "ButtonStatus\n"
        p += "isPressed : {}".format(self.is_pressed)


class ButtonInd(IntEnum):
    USER_BTN = 0


class IMUData(GenericData):
    def __init__(self):
        super().__init__()
        self.rotation = None
        self.acceleration = None
        self.angular_velocity = None

    def __repr__(self):
        p = "IMUData"
        p += super().__repr__()
        p += self.rotation.__repr__()
        p += self.acceleration.__repr__()
        p += self.angular_velocity.__repr__()
        return p


class TargetMotion(GenericData):
    def __init__(self):
        super().__init__()
        self.rotation = None
        self.velocity = None

    def __repr__(self):
        p = "TargetMotion"
        p += super().__repr__()
        p += self.rotation.__repr__()
        p += self.velocity.__repr__()
        return p


class SonarData(GenericData):
    def __init__(self):
        super().__init__()
        self.dist = None

    def __repr__(self):
        p = "SonarData\n"
        p += super().__repr__()
        p += "distance : {}".format(self.dist)
        return p


class DepthData(GenericData):
    def __init__(self, depth=None):
        super().__init__()
        self.depth = depth


class CurrentDepth(DepthData):
    def __init__(self, depth=None):
        super().__init__(depth)


class TargetDepth(DepthData):
    def __init__(self, depth=None):
        super().__init__(depth)

    def __repr__(self):
        p = "DepthData\n"
        p += super().__repr__()
        p += "depth : {}".format(self.depth)
        return p


class ImgData(GenericData):
    def __init__(self):
        super().__init__()
        self.raw = None
        self.jpg_encoded = None

    def __repr__(self):
        p = "ImgData\n"
        p += super().__repr__()
        return p


class ThrusterStatus(GenericData):
    """
    Attributes:
        speed(`list` of `int`): speed of each thruster in a list
    """
    def __init__(self, speed=None):
        super().__init__()
        self.speed = speed

    def __repr__(self):
        p = "ThrusterStatus\n"
        p += super().__repr__()
        p += "Thruster speeds : {}".format(self.speed)
        return p


class PIDValue(GenericData):
    def __init__(self, p=None, i=None, d=None):
        super().__init__()
        self.p = p
        self.i = i
        self.d = d

    def __repr__(self):
        p = "PIDValue\n"
        p += super().__repr__()
        p += "p : {}, i : {}, d : {}".format(self.p, self.i, self.d)
        return p


class BatteryStatus(GenericData):
    """
    Attributes:
        voltage (`list` of `float`):  battery voltage in a list
        percentage (`list` of `int`): battery percentage in a list
    """
    def __init__(self, voltage=None, percentage=None):
        super().__init__()
        self.voltage = voltage
        self.percentage = percentage

    def __repr__(self):
        p = "BatteryStatus\n"
        p += super().__repr__()
        p += "voltage : {}, percentage : {}".format(self.voltage, self.percentage)
        return p


class ErrorStatus(Enum):
    NO_ERROR = 0
    PERMISSION_DENIED = 1
    NO_IMPLEMENTATION = 2
    DATA_UNAVAILABLE = 3
    WRONG_DATA_TYPE = 4
