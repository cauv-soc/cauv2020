from PythonComponent.shared.gRPC.clientInterface import ClientInterface
from PythonComponent.shared.gRPC.containers import *
import PythonComponent.shared.gRPC.communication_pb2 as communication_pb2
import PythonComponent.shared.gRPC.communication_pb2_grpc as communication_pb2_grpc
from PythonComponent.shared.gRPC.utilities.protobufObjectUtilities import *
from PythonComponent.shared.utilities.timeStamp import curr_time
import threading
import logging
import grpc
import time


class GrpcClient(ClientInterface):
    def __init__(self, channelName='localhost:50051', updateFreq_ms=20):
        self.channelName = channelName
        self.updateFreq_ms = updateFreq_ms

        # downstream instruction
        self.placeholder = communication_pb2.PlaceHolder()
        self.instruction_pack = communication_pb2.InstructionPack()
        self.sensor_data_pack = communication_pb2.SensorDataPack()

        # used to block the set functions when instruction_pack is in use
        self.write_lock = threading.Lock()
        self.error_status = None  # not sure how to deal sigh it yet
        # used to block the read functions when sensor_data_pack is updated
        self.read_lock = threading.Lock()

        self.ready = False
        self.start_time = curr_time()
        self.thread = threading.Thread(target=self.update)

    def run(self):
        self.thread.daemon = True
        self.thread.start()

    def update(self):
        logger = logging.getLogger(__name__)
        logger.info('creating channel@ ' + self.channelName)
        # context manager
        with grpc.insecure_channel(self.channelName) as channel:
            stub = communication_pb2_grpc.grpcCommunicationStub(channel)
            logger.info('channel and stub created')
            while True:
                logger.debug('send out instruction')
                with self.write_lock:
                    stub.send_instruction(self.instruction_pack)
                    self.instruction_pack = communication_pb2.InstructionPack()

                logger.debug('retrieving information')
                received_data = stub.read_sensors_all(self.placeholder)
                with self.read_lock:
                    self.sensor_data_pack = merge_objects(self.sensor_data_pack, received_data)

                # this controls the update rate
                time.sleep(self.updateFreq_ms/1000)

    # for all the functions, it is important to set the time_stamp
    # otherwise, it might not be handled correctly but the client
    def set_velocity(self, velocity):
        if not isinstance(velocity, Velocity):
            return ErrorStatus.WRONG_DATA_TYPE
        with self.write_lock:
            copy_python_vec3_to_grpc(self.instruction_pack.target_velocity, velocity)
        return ErrorStatus.NO_ERROR

    def set_rotation(self, rotation):
        if not isinstance(rotation, Rotation):
            return ErrorStatus.WRONG_DATA_TYPE
        with self.write_lock:
            copy_python_vec3_to_grpc(self.instruction_pack.target_rotation, rotation)
        return ErrorStatus.NO_ERROR

    def set_light_brightness(self, light_index, light_brightness):
        if light_index == LightInd.MBED_LED_1:
            self.instruction_pack.target_mbed_led_1.brightness = light_brightness
            self.instruction_pack.target_mbed_led_1.time_stamp = curr_time()
            return ErrorStatus.NO_ERROR
        if light_index == LightInd.MBED_LED_2:
            self.instruction_pack.target_mbed_led_2.brightness = light_brightness
            self.instruction_pack.target_mbed_led_2.time_stamp = curr_time()
            return ErrorStatus.NO_ERROR
        if light_index == LightInd.MBED_LED_3:
            self.instruction_pack.target_mbed_led_3.brightness = light_brightness
            self.instruction_pack.target_mbed_led_3.time_stamp = curr_time()
            return ErrorStatus.NO_ERROR
        return ErrorStatus.NO_IMPLEMENTATION

    def set_pid_value(self, pid_value):
        if not isinstance(pid_value, PIDValue):
            return ErrorStatus.WRONG_DATA_TYPE
        self.instruction_pack.target_pid.p = pid_value.p
        self.instruction_pack.target_pid.i = pid_value.i
        self.instruction_pack.target_pid.d = pid_value.d
        self.instruction_pack.target_pid.time_stamp = curr_time()
        return ErrorStatus.NO_ERROR

    # getters
    def read_target_velocity(self):
        with self.read_lock:
            velocity = Velocity()
            if self.sensor_data_pack.target_velocity.time_stamp == 0:
                return ErrorStatus.DATA_UNAVAILABLE

            copy_grpc_vec3_to_python(self.sensor_data_pack.target_velocity, velocity)
            return velocity

    def read_target_rotation(self):
        with self.read_lock:
            rotation = Rotation()
            if self.sensor_data_pack.target_rotation.time_stamp == 0:
                return ErrorStatus.DATA_UNAVAILABLE
            copy_grpc_vec3_to_python(self.sensor_data_pack.target_rotation, rotation)
            return rotation

    def read_light_brightness(self):
        return ErrorStatus.NO_IMPLEMENTATION

    # note that the acceleration and angular velocity parts are not implemented yet
    def read_imu(self):
        with self.read_lock:
            imu_data = IMUData()
            rotation = Rotation()
            if self.sensor_data_pack.current_rotation.time_stamp == 0:
                return ErrorStatus.DATA_UNAVAILABLE
            imu_data.time_stamp = self.sensor_data_pack.current_rotation.time_stamp
            copy_grpc_vec3_to_python(self.sensor_data_pack.current_rotation, rotation)
            imu_data.rotation = rotation
            return imu_data

    def read_thruster_status(self):
        with self.read_lock:
            if self.sensor_data_pack.thruster_status.time_stamp == 0:
                return ErrorStatus.DATA_UNAVAILABLE
            thruster_status = ThrusterStatus()
            thruster_status.speed = [self.sensor_data_pack.thruster_status.thruster1,
                                     self.sensor_data_pack.thruster_status.thruster2,
                                     self.sensor_data_pack.thruster_status.thruster3,
                                     self.sensor_data_pack.thruster_status.thruster4,
                                     self.sensor_data_pack.thruster_status.thruster5,
                                     self.sensor_data_pack.thruster_status.thruster6]
            thruster_status.time_stamp = self.sensor_data_pack.thruster_status.time_stamp
            return thruster_status

    def read_button_status(self):
        with self.read_lock:
            if self.sensor_data_pack.user_btn.time_stamp == 0:
                return ErrorStatus.DATA_UNAVAILABLE
            button_status = ButtonStatus()
            button_status.is_pressed = [self.sensor_data_pack.user_btn.pressed]
            button_status.time_stamp = [self.sensor_data_pack.user_btn.time_stamp]
            return button_status

    def read_depth(self):
        with self.read_lock:
            if self.sensor_data_pack.depth.time_stamp == 0:
                return ErrorStatus.DATA_UNAVAILABLE
            depth_data = DepthData()
            depth_data.depth = self.sensor_data_pack.depth.value
            depth_data.time_stamp = self.sensor_data_pack.depth.time_stamp
            return depth_data

    def read_sonar(self):
        return ErrorStatus.NO_IMPLEMENTATION

    def read_img(self):
        return ErrorStatus.NO_IMPLEMENTATION

    def read_battery_status(self):
        with self.read_lock:
            if self.sensor_data_pack.battery_1.time_stamp == 0:
                return ErrorStatus.DATA_UNAVAILABLE
            battery_status = BatteryStatus()
            battery_status.voltage = [self.sensor_data_pack.battery_1.voltage,
                                      self.sensor_data_pack.battery_2.voltage]
            battery_status.percentage = [self.sensor_data_pack.battery_1.percentage,
                                         self.sensor_data_pack.battery_2.percentage]
            battery_status.time_stamp = self.sensor_data_pack.battery_1.time_stamp
            return battery_status

    # other functions
    def emergency_stop(self):
        return ErrorStatus.NO_IMPLEMENTATION

    def hardware_reset(self):
        return ErrorStatus.NO_IMPLEMENTATION
