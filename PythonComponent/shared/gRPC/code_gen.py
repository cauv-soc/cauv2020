from grpc_tools import protoc

protoc.main((
    '',
    '-I.',
    '--python_out=.',
    '--grpc_python_out=.',
    'communication.proto',
))

grpc_file = open("communication_pb2_grpc.py", "r")
all_lines = grpc_file.readlines()
all_lines[3] = "import PythonComponent.shared.gRPC.communication_pb2 as communication__pb2"
grpc_file.close()
grpc_file = open("communication_pb2_grpc.py", "w")
for line in all_lines:
    grpc_file.write(line)
grpc_file.close()