import logging
import pickle

import PythonComponent.shared.gRPC.communication_pb2 as communication_pb2
import PythonComponent.shared.gRPC.communication_pb2_grpc as communication_pb2_grpc
import cv2
import numpy as np
from PythonComponent.shared.gRPC.containers import *
from PythonComponent.shared.utilities.queueUtilities import *
from PythonComponent.shared.gRPC.containers import *
from PythonComponent.shared.gRPC.utilities.protobufObjectUtilities import *
from PythonComponent.shared.utilities.timeStamp import curr_time
from PythonComponent.shared.utilities.printUtil import *


class CommunicationServicer(communication_pb2_grpc.grpcCommunicationServicer):
    def __init__(self, image_streaming_q, gs_instruction_q, status_update_q):
        # assume that things will be set properly after the constructor
        self.image_streaming_q = image_streaming_q
        self.gs_instruction_q = gs_instruction_q
        self.status_update_q = status_update_q

    def echo(self, request, context):
        logger = logging.getLogger(__name__)
        logger.info('Communication server received: ' + request.Msg)
        return request

    def read_sensors_all(self, request, context):
        sensor_data_pack = communication_pb2.SensorDataPack()
        logger = logging.getLogger(__name__)
        # logger.debug('read_sensors_all called ')
        while True:
            try:
                container = self.status_update_q.get_nowait()
                # logger.debug(str_class(container))
                if isinstance(container, CurrentVelocity):
                    copy_python_vec3_to_grpc(sensor_data_pack.current_velocity, container)
                    continue
                if isinstance(container, TargetVelocity):
                    copy_python_vec3_to_grpc(sensor_data_pack.target_velocity, container)
                    continue
                if isinstance(container, CurrentAcceleration):
                    copy_python_vec3_to_grpc(sensor_data_pack.current_acceleration, container)
                    continue
                if isinstance(container, CurrentRotation):
                    copy_python_vec3_to_grpc(sensor_data_pack.current_rotation, container)
                    continue
                if isinstance(container, TargetRotation):
                    copy_python_vec3_to_grpc(sensor_data_pack.target_rotation, container)
                    continue
                if isinstance(container, CurrentAngularVelocity):
                    copy_python_vec3_to_grpc(sensor_data_pack.current_angular_velocity, container)
                    continue
                if isinstance(container, TargetAngularVelocity):
                    copy_python_vec3_to_grpc(sensor_data_pack.target_angular_velocity, container)
                    continue
                if isinstance(container, CurrentDepth):
                    sensor_data_pack.depth.value = container.depth
                    sensor_data_pack.depth.time_stamp = container.time_stamp
                    continue
                if isinstance(container, ButtonStatus):
                    sensor_data_pack.user_btn.pressed = container.is_pressed[0]
                    sensor_data_pack.user_btn.time_stamp = container.time_stamp
                    continue
                if isinstance(container, BatteryStatus):
                    sensor_data_pack.battery_1.voltage = container.voltage[0]
                    sensor_data_pack.battery_1.percentage = container.percentage[0]
                    sensor_data_pack.battery_1.time_stamp = container.time_stamp
                    continue
                logger = logging.getLogger(__name__)
                logger.info('class not recognized:  ' + container.__class__.__name__)
            except queue.Empty:
                break
        # logger.debug(str_grpc_class(sensor_data_pack, hide_unavailable_data=True))
        return sensor_data_pack

    def send_instruction(self, request, context):
        logger = logging.getLogger(__name__)
        # logger.debug('send instruction called')
        if not (request.target_velocity.time_stamp == 0):
            target_velocity = TargetVelocity()
            copy_grpc_vec3_to_python(request.target_velocity, target_velocity)
            self.gs_instruction_q.put(target_velocity)

        if not (request.target_rotation.time_stamp == 0):
            target_rotation = TargetRotation()
            copy_grpc_vec3_to_python(request.target_rotation, target_rotation)
            self.gs_instruction_q.put(target_rotation)

        if not (request.target_head_light_1.time_stamp == 0):
            logger.info("target_head_light_1 not implemented")

        if not (request.target_head_light_2.time_stamp == 0):
            logger.info("target_head_light_2 not implemented")

        if not (request.target_head_light_3.time_stamp == 0):
            logger.info("target_head_light_3 not implemented")

        if not (request.target_head_light_4.time_stamp == 0):
            logger.info("target_head_light_4 not implemented")

        if not (request.target_mbed_led_1.time_stamp == 0):
            target_light_brightness = TargetLightBrightness()
            target_light_brightness.index = LightInd.MBED_LED_1  # need to think about how to deal with this...
            target_light_brightness.brightness = request.target_mbed_led_1.brightness
            target_light_brightness.time_stamp = request.target_mbed_led_1.time_stamp
            self.gs_instruction_q.put(target_light_brightness)

        if not (request.target_mbed_led_2.time_stamp == 0):
            logger.info("target_mbed_led2 not implemented")

        if not (request.target_mbed_led_3.time_stamp == 0):
            logger.info("target_mbed_led2 not implemented")

        if not (request.target_pid.time_stamp == 0):
            logger.info("target_pid not implemented")

        return communication_pb2.ErrorStatus()

    def request_image(self, request, context):
        curr_img = read_latest(self.image_streaming_q)
        image = communication_pb2.Image()
        image.pickle_dumped_raw_img = pickle.dumps(curr_img)
        image.time_stamp = curr_time()
        return image
