# gPRC
## To-do
*   Add something to code_gen to deal with the import path issue
*   videoCapture Async flush till the lasted frame function
*   Find a way to integrate the display with Qt GUI
*   Client class test done, but many other minor fixes to be dealt with
## Installation Guide
https://grpc.io/docs/quickstart/python/

```
sudo python -m pip install grpcio
python -m pip install grpcio-tools
```

## Code gen
https://github.com/grpc/grpc/blob/master/tools/distrib/python/grpcio_tools/grpc_tools/protoc.py

Now the import path is not working very nicely, so need to handle it manually