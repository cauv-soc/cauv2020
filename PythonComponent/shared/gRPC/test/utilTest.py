import PythonComponent.shared.gRPC.communication_pb2 as communication_pb2
from PythonComponent.shared.gRPC.utilities.protobufObjectUtilities import *
from PythonComponent.shared.gRPC.containers import *

#####################################
# test for MergeFrom                #
#####################################
# p = communication_pb2.SensorDataPack()
# light_status = communication_pb2.LightStatus()
# light_status.brightness = 100
# light_status.time_stamp = 2
# p.mbed_led_3.MergeFrom(light_status)
#
# output = communication_pb2.SensorDataPack()
#
# result = merge_objects(output, p)
#
# print(dir(result))
# print(getattr(result, 'mbed_led_3').time_stamp)

#####################################
# test for copy to python           #
#####################################
# vec = communication_pb2.Vec3D()
# vec.x = 1
# vec.y = 2
# vec.z = 3
# vec.time_stamp = 1
#
# rotation = Rotation()
# rotation = copy_vec3_to_python(vec, rotation)
# print(rotation[0], rotation[1], rotation[2])
#
# rotation = Velocity()
# rotation = copy_vec3_to_python(vec, rotation)
# print(rotation[0], rotation[1], rotation[2])
#
# rotation = Acceleration()
# rotation = copy_vec3_to_python(vec, rotation)
# print(rotation[0], rotation[1], rotation[2])
#
# rotation = AngularVelocity()
# rotation = copy_vec3_to_python(vec, rotation)
# print(rotation[0], rotation[1], rotation[2])


#####################################
# test for copy to grpc           #
#####################################

rotation = Rotation(1, 2, 3)
rotation.time_stamp = 1
p = communication_pb2.SensorDataPack()
copy_python_vec3_to_grpc(p.current_rotation, rotation)
print(p.current_rotation.time_stamp)
