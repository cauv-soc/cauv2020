import cv2
import pickle
import time
from PythonComponent.shared.gRPC.containers import *

statusReport = StatusReportContainer()
img = cv2.imread('testImg.jpg')
statusReport.imgFrameRaw = img
serialized = pickle.dumps(statusReport)
print(len(serialized))
recovered = pickle.loads(serialized)
if not isinstance(recovered, StatusReportContainer):
    print("error")
else:
    cv2.imshow('test', recovered.imgFrameRaw)
    cv2.waitKey(0)
    time.sleep(100)
