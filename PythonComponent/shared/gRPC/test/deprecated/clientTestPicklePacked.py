import PythonComponent.shared.gRPC.communication_pb2 as communication_pb2
import PythonComponent.shared.gRPC.communication_pb2_grpc as communication_pb2_grpc
import logging
import grpc
import time
import cv2
import pickle
from PythonComponent.shared.gRPC.containers import *


def run():
    port = 'localhost:50051'
    port = "192.168.142.113:50051"
    with grpc.insecure_channel(port) as channel:
        stub = communication_pb2_grpc.CommunicationStub(channel)
        myMsg = communication_pb2.HelloWorldMsg(Msg="HelloWorld")
        myMsg2 = communication_pb2.genericPickleMsg()
        logging.info("My message generated")
        while True:
            returnMsg = stub.Echo(myMsg)
            logging.info("Message Received: " + returnMsg.Msg)
            # time.sleep(1)

            returnMsg = stub.statusReport(myMsg2)
            statusReport = pickle.loads(returnMsg.data)
            if not isinstance(statusReport, StatusReportContainer):
                print("error")
            else:
                if not(statusReport.imgFrameRaw is None):
                    cv2.imshow("img_decode", statusReport.imgFrameRaw)
                    cv2.waitKey(1)
            time.sleep(0.01)


logging.basicConfig(level=logging.DEBUG)
run()
