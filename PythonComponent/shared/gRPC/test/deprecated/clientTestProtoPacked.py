import PythonComponent.shared.gRPC.communication_pb2 as communication_pb2
import PythonComponent.shared.gRPC.communication_pb2_grpc as communication_pb2_grpc
import logging
import grpc
import time
import cv2
import numpy as np


def run():
    with grpc.insecure_channel('10.240.152.57:50051') as channel:
        stub = communication_pb2_grpc.CommunicationStub(channel)
        myMsg = communication_pb2.HelloWorldMsg(Msg="HelloWorld")
        myMsg2 = communication_pb2.PlaceHolder()
        logging.info("My message generated")
        while True:
            returnMsg = stub.Echo(myMsg)
            logging.info("Message Received: " + returnMsg.Msg)
            # time.sleep(1)
            returnMsg = stub.StatusReport(myMsg2)

            # unpack the message
            if returnMsg.encodedImg == b'':
                logging.info('empty string received')
            else:
                npArr = np.frombuffer(returnMsg.encodedImg, np.uint8)
                decodeSuccess = False
                try:
                    img_decode = cv2.imdecode(npArr, cv2.IMREAD_COLOR)
                    decodeSuccess = True
                except:
                    logging.info('decodeError')
                if decodeSuccess:
                    cv2.imshow("img_decode", img_decode)
                    cv2.waitKey(1)
            time.sleep(0.00001)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    run()
