from PythonComponent.shared.gRPC.deprecated.client import Client
import cv2
from PythonComponent.shared.utilities.findIP import get_ip
import logging


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    myIP = get_ip()
    port = myIP + ':50051'
    print(port)
    client = Client(channelName=port, updateFreq_ms=50)
    client.run()

    while True:
        statusReport = client.statusUpdateQueue.get()
        if not (statusReport.imgFrameRaw is None):
            cv2.imshow("img_decode", statusReport.imgFrameRaw)
            cv2.waitKey(1)
        else:
            logging.info('problem?')
