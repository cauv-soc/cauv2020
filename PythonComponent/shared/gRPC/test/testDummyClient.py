import logging
import time
import sys
sys.path.append('C:/Users/Daniel/Documents/CAUV/repo')

from PythonComponent.shared.gRPC.dummyClient import DummyClient
from PythonComponent.shared.gRPC.containers import *

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    log = logging.getLogger(__name__)

    log.info("===== Demo of retrieving data from dummyClient =====")

    client = DummyClient()
    log.info("made client object")

    log.info("starting value sweeping")
    client.start_sweep()

    N = 100
    for i in range(N):
        # call all the functions available to ensure they do not throw an error
        imu_val = client.read_imu()
        log.info(imu_val)

        log.info(client.read_target_velocity())
        client.read_target_rotation()
        log.info(client.read_light_brightness())
        client.read_thruster_status()
        client.read_button_status()
        client.read_depth()
        client.read_sonar()
        client.read_img()
        log.info(client.read_battery_status())

        client.set_velocity(TargetVelocity(vx=1.0, vy=2.0, vz=3.0))
        client.set_rotation(TargetRotation(roll=10.0, pitch=-20.0, yaw=47.0))

        time.sleep(0.1)

    # check that the system returns an error class if the wrong data type is supplied
    assert(client.set_velocity(10.0) == ErrorStatus.WRONG_DATA_TYPE)
    assert(client.set_rotation(Velocity(vx=1.0, vy=2.0, vz=3.0)) == ErrorStatus.WRONG_DATA_TYPE)

    client.end_sweep()

    """
    N = 1000
    log.info("reading data which has been collected")
    for i in range(N):
        if data is not None:
            pass
            # For some strange reason, note that when printing a protobuf object directly,
            # fields with values of 0 are omitted. They are still present in the object and
            # may be accessed in the same way.
            log.info(data)
        else:
            log.info("No data received yet")
        time.sleep(0.1)

    log.info("stopping value sweeping")
    client.end_sweep()
    """
