import PythonComponent.shared.gRPC.communication_pb2 as communication_pb2
p = communication_pb2.PlaceHolder()
print(p.Msg)
if p.Msg is "":
    print('it is indeed an empty string')

print('normal message')
p = communication_pb2.Vec3D()
print(p.timeStamp)
print(p.x)

print('nested message')
p = communication_pb2.SensorDataPack()
print(p.current_speed.x)
print(p.current_speed.timeStamp)

# result of the test:
# protobuf does not do None
# everything gets initialized
# the only way to check if a field is present is to use time stamp
# for consistency, a zero time stamp means that the field is not initialized
