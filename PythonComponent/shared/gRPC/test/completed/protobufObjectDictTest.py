import PythonComponent.shared.gRPC.communication_pb2 as communication_pb2
p = communication_pb2.SensorDataPack()
light_status = communication_pb2.LightStatus()
light_status.brightness = 100
light_status.time_stamp = 2
p.mbed_led_3.MergeFrom(light_status)

output = communication_pb2.SensorDataPack()
for field in dir(p):
    field_data = getattr(p, field)
    if hasattr(field_data, 'time_stamp'):
        if not (field_data.time_stamp == 0):
            getattr(output, field).MergeFrom(field_data)

print(dir(p))
print(getattr(p, 'mbed_led_3').time_stamp)

print(dir(output))
print(dir(output) == dir(p))
