from threading import Thread
import logging
import time

from PythonComponent.shared.gRPC.test.grpcTestWithDummy.dummyMainLogic import DummyMainLogic
from PythonComponent.shared.gRPC.containers import *
from PythonComponent.shared.utilities.timeStamp import curr_time


def dummy_grpc(dummy_main):
    # runs for 5 seconds, printing any data from dummy main
    # and putting in some generic data

    start = time.time()
    last_read = time.time()
    while (time.time() - start) < 5:
        period = 1
        if (time.time() - last_read) > period:
            last_read = time.time()
            test_data = GenericData()
            test_data.timestamp = curr_time()
            test_data.msg = "Testing dummyMainLogic class"
            log.info(test_data)
            print(test_data)

            dummy_main.recv_queue.put(test_data)

            while not dummy_main.send_queue.empty():
                message = dummy_main.send_queue.get(block=False)
                if message:
                    log.info("From mainLogic:")
                    log.info(message)
                    log.info("----")
                else:
                    break

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    log = logging.getLogger(__name__)

    dummy_main = DummyMainLogic(1)

    t = Thread(target=dummy_grpc, args=(dummy_main,), daemon=True)

    log.info("Starting both loops - should see GRPC container classes printed ")
    t.start()
    dummy_main.main_loop()
