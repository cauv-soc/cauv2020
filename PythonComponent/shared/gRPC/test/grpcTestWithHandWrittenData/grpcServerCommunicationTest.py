# it needs to define the queues

from PythonComponent.shared.gRPC.grpcCommunicationServicer import CommunicationServicer
from PythonComponent.shared.gRPC.server import Server
from queue import Queue, Empty
from PythonComponent.shared.gRPC.test.grpcTestWithHandWrittenData.grpcCommunicationServicerBareTest import populate_status_update_q
import time
import logging
from PythonComponent.shared.utilities.printUtil import *

logging.basicConfig(level=logging.INFO)
logging.debug("running")
# set up the information passing queues
gs_instruction_q = Queue()
gs_img_streaming_q = Queue()
gs_status_update_q = Queue()
img_proc_streaming_q = Queue()
mbed_status_update_q = Queue()

# set up the server
servicer = CommunicationServicer(gs_img_streaming_q, gs_instruction_q, gs_status_update_q)
server = Server(servicer)
server.start()

populate_status_update_q(gs_status_update_q)
# serve()
while True:
    try:
        container = gs_instruction_q.get_nowait()
        logging.info('found')
        logging.info(str_class(container))
    except Empty:
        time.sleep(0.5)




