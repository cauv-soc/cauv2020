from PythonComponent.shared.gRPC.containers import *
from queue import Queue
from PythonComponent.shared.gRPC.grpcCommunicationServicer import CommunicationServicer
import logging


def populate_status_update_q(status_update_q):
    curr_rot = CurrentRotation(1, 2, 3)
    curr_rot.time_stamp = 1

    tar_rot = TargetRotation(2, 3, 4)
    tar_rot.time_stamp = 2

    curr_vel = CurrentVelocity(4, 5, 6)
    curr_vel.time_stamp = 3

    btn_status = ButtonStatus()
    btn_status.is_pressed = [1]
    btn_status.time_stamp = 4

    depth = CurrentDepth()
    depth.depth = 10
    depth.time_stamp = 5

    tar_vel = TargetVelocity()
    tar_vel.time_stamp = 6
    tar_vel.vx = 10
    tar_vel.vy = 11
    tar_vel.vz = 12

    unknown = LightBrightness()

    status_update_q.put(curr_rot)
    status_update_q.put(tar_rot)
    status_update_q.put(btn_status)
    status_update_q.put(depth)
    status_update_q.put(unknown)
    status_update_q.put(tar_vel)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    logging.debug("running")

    gs_status_update_q = Queue()
    populate_status_update_q(gs_status_update_q)

    servicer = CommunicationServicer(None, None, gs_status_update_q)
    ret = servicer.read_sensors_all(None, None)
    q = 1
    p = 2
    z = 3
    print(q + p + z)
