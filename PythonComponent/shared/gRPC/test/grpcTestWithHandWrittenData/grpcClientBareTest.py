from PythonComponent.shared.gRPC.grpcClient import GrpcClient
import PythonComponent.shared.gRPC.communication_pb2 as communication_pb2
from PythonComponent.shared.gRPC.containers import *
client = GrpcClient()

vec = communication_pb2.Vec3D()
vec.x = 1
vec.y = 2
vec.z = 3
vec.time_stamp = 1
client.sensor_data_pack.target_velocity.MergeFrom(vec)

vec.x = 2
vec.y = 3
vec.z = 4
vec.time_stamp = 2
client.sensor_data_pack.target_rotation.MergeFrom(vec)

vec.x = 1
vec.y = 2
vec.z = 3
vec.time_stamp = 4
client.sensor_data_pack.current_velocity.MergeFrom(vec)

vec.x = 2
vec.y = 3
vec.z = 4
vec.time_stamp = 5
client.sensor_data_pack.current_rotation.MergeFrom(vec)

thruster_status = communication_pb2.ThrusterStatus()
thruster_status.thruster1 = 1
thruster_status.thruster2 = 2
thruster_status.thruster3 = 3
thruster_status.thruster4 = 4
thruster_status.thruster5 = 5
thruster_status.thruster6 = 6
thruster_status.time_stamp = 6
client.sensor_data_pack.thruster_status.MergeFrom(thruster_status)


output1 = client.read_target_velocity()
print(output1.time_stamp)
output2 = client.read_target_rotation()
print(output2.time_stamp)
output3 = client.read_imu()
print(output3.time_stamp)
output4 = client.read_thruster_status()
print(output4.time_stamp)


rotation = Rotation()
rotation[0] = 7
rotation[1] = 8
rotation[2] = 9
rotation.time_stamp = 10

velocity = Velocity()
velocity[0] = 11
velocity[1] = 12
velocity[2] = 13
velocity.time_stamp = 14

client.set_velocity(velocity)
client.set_rotation(rotation)

print(1)





