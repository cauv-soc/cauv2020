from PythonComponent.shared.gRPC.grpcClient import GrpcClient
import PythonComponent.shared.gRPC.communication_pb2 as communication_pb2
from PythonComponent.shared.gRPC.containers import *
import logging
from PythonComponent.shared.utilities.printUtil import *
import time
logging.basicConfig(level=logging.DEBUG)
logging.debug("running")
client = GrpcClient(updateFreq_ms=500)
client.run()

logging.info('give it a wait for the client to set up')
time.sleep(2)
logging.info('wait completed, trying to acquire data now')

for item in dir(client):
    func = getattr(client, item)
    if not callable(func):
        continue
    if not ('read' in item):
        continue
    print(item)
    output = func()
    if isinstance(output, ErrorStatus):
        print(output._name_)
    else:
        print_class(output)

tar_rot = TargetRotation(2, 3, 4)
tar_rot.time_stamp = 2
tar_vel = TargetVelocity(10, 11, 12)
tar_vel.time_stamp = 6

client.set_rotation(tar_rot)
client.set_velocity(tar_vel)
client.set_light_brightness(light_index=LightInd.MBED_LED_1, light_brightness=10)

time.sleep(100)
# print(dir(client))
# print_class(client.read_target_velocity())
# print_class(client.read_target_rotation())
# print_class(client.read_light_brightness())
# print_class(client.read_imu())
# print_class(client.read_thruster_status())
# print_class(client.read_button_status())
# print_class(client.read_depth())
# print_class(client.read_sonar())
