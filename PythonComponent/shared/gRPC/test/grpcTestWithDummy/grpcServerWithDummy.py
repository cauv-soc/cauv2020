from PythonComponent.shared.gRPC.grpcCommunicationServicer import CommunicationServicer
from PythonComponent.shared.gRPC.server import Server
from queue import Empty
from PythonComponent.shared.utilities.printUtil import *
from PythonComponent.shared.gRPC.test.grpcTestWithDummy.dummyMainLogic import *
from threading import Thread

logging.basicConfig(level=logging.INFO)
logging.debug("running")
dummy_main = DummyMainLogic(1)
gs_img_streaming_q = Queue()
gs_status_update_q = dummy_main.send_queue  # dummy_main.send_queue
gs_instruction_q = Queue()
servicer = CommunicationServicer(gs_img_streaming_q, gs_instruction_q, gs_status_update_q)
server = Server(servicer)
server.start()

t = Thread(target=dummy_main.main_loop, daemon=True)
logging.debug("dummy_main.main_loop starting")
t.start()
logging.info("dummy_main.main_loop started")

# serve()
while True:
    try:
        container = gs_instruction_q.get_nowait()
        logging.info('found')
        logging.info(str_class(container))
    except Empty:
        time.sleep(0.5)
