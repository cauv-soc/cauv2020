from queue import Queue
import time
import logging
import math

from PythonComponent.shared.gRPC.containers import *
from PythonComponent.shared.utilities import timeStamp


class DummyMainLogic:

    def __init__(self, loop_period=0.05):
        self.loop_period = loop_period
        self.log = logging.getLogger(__name__)

        self.send_queue = Queue()
        self.recv_queue = Queue()

        self.imu_data = IMUData()
        self.imu_data.rotation = CurrentRotation()
        self.imu_data.acceleration = CurrentAcceleration()
        self.imu_data.angular_velocity = CurrentAngularVelocity()

        self.sonar_data = SonarData()
        self.depth_data = CurrentDepth()
        self.img_data = ImgData()
        self.thrusters_status = None
        self.battery_status = BatteryStatus()

        self.set_example_data()

        # sweep variables
        self.sweep = True
        self.sweep_start_t = 0

        self.THRUSTER_RANGE = (0, 100)
        self.THRUSTER_SWEEP_SPEED = 1
        self.NUM_THRUSTER = 6

        self.ANGLE_RANGE = (-180.0, 180.0)
        self.PITCH_RANGE = (-90.0, 90.0)
        self.ANGLE_SWEEP_SPEED = 1

        self.SONAR_RANGE = (0.0, 100.0)
        self.SONAR_SWEEP_SPEED = 1

        self.DEPTH_RANGE = (0.0, 50.0)
        self.DEPTH_SWEEP_SPEED = 1

        self.VOLTAGE_RANGE = (0.0, 16.8)
        self.VOLTAGE_SWEEP_SPEED = 1

        self.NUM_LIGHTS = 4
        self.lights = [LightBrightness(index=i, brightness=0) for i in range(self.NUM_LIGHTS)]

    def main_loop(self):
        # 20Hz update rate for sending telemetry back
        # likely need to adjust according to gRPC speed
        loop_period = self.loop_period
        last_send = time.time()

        while True:
            # send fake telemetry data at regular intervals of loop_period
            if (time.time() - last_send) >= loop_period:
                last_send = time.time()
                self.test_sweep_parameters()
                self.all_data_to_queue()

            # read all items in the queue and print them out
            while not self.recv_queue.empty():
                message = self.recv_queue.get(block=False)
                if message:
                    self.log.info(message)
                else:
                    break

    def all_data_to_queue(self):
        self.send_queue.put(self.imu_data.rotation)
        # self.send_queue.put(self.sonar_data)
        self.send_queue.put(self.depth_data)
        self.send_queue.put(self.battery_status)
        self.send_queue.put(self.lights)
        # self.send_queue.put(self.thrusters_status)

    def test_sweep_parameters(self, thrusters=True, orientation=True, sonar=True, pressure=True, battery=True):
        if thrusters:
            for i, thruster in enumerate(self.thrusters_status):
                thruster.speed = self.sweep_function(self.THRUSTER_RANGE,
                                                     self.THRUSTER_SWEEP_SPEED,
                                                     2 * math.pi * i / self.NUM_THRUSTER)
                thruster.time_stamp = timeStamp.curr_time()

        if orientation:
            self.imu_data.rotation.roll = self.sweep_function(self.ANGLE_RANGE, self.ANGLE_SWEEP_SPEED, 0)
            self.imu_data.rotation.pitch = self.sweep_function(self.PITCH_RANGE, self.ANGLE_SWEEP_SPEED,
                                                               2 * math.pi / 3)
            self.imu_data.rotation.yaw = self.sweep_function(self.ANGLE_RANGE, self.ANGLE_SWEEP_SPEED, 4 * math.pi / 3)
            self.imu_data.rotation.time_stamp = timeStamp.curr_time()
            self.imu_data.time_stamp = timeStamp.curr_time()

        if sonar:
            self.sonar_data.dist = self.sweep_function(self.SONAR_RANGE, self.SONAR_SWEEP_SPEED)
            self.imu_data.time_stamp = timeStamp.curr_time()

        if pressure:
            self.depth_data.depth = self.sweep_function(self.DEPTH_RANGE, self.DEPTH_SWEEP_SPEED)
            self.depth_data.time_stamp = timeStamp.curr_time()

        if battery:
            self.battery_status.voltage = [self.sweep_function(self.VOLTAGE_RANGE, self.VOLTAGE_SWEEP_SPEED)]
            self.battery_status.percentage = [(self.battery_status.voltage[0] / self.VOLTAGE_RANGE[1]) * 100]
            self.battery_status.time_stamp = timeStamp.curr_time()

    def set_example_data(self):
        # sets some random plausible data for the system,
        # useful for debugging individual GUI issues and ensuring
        # all data is obtained correctly
        current_timestamp = timeStamp.curr_time()

        thruster_values = [20, 53, 34, 67, 81, 14]
        self.thrusters_status = [ThrusterStatus(speed=i) for i in thruster_values]
        for thruster in self.thrusters_status:
            thruster.time_stamp = current_timestamp

        self.imu_data.rotation.roll = 35.0
        self.imu_data.rotation.pitch = -20.0
        self.imu_data.rotation.yaw = 146.0
        self.imu_data.time_stamp = current_timestamp
        self.imu_data.rotation.time_stamp = current_timestamp

        self.sonar_data.dist = 28.0
        self.sonar_data.time_stamp = current_timestamp
        self.depth_data.depth = 5.8
        self.depth_data.time_stamp = current_timestamp

        self.battery_status.voltage = [16.3]
        self.battery_status.percentage = [82]
        self.battery_status.time_stamp = current_timestamp

    def sweep_function(self, range, omega, phase = 0):
        """sweeps values based on the sin function"""
        t = self.sweep_start_t - time.time()
        val = ((range[1] - range[0]) * math.sin(t * omega + phase) + (range[1] + range[0]))/2
        return val
