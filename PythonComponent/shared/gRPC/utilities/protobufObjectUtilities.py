import PythonComponent.shared.gRPC.communication_pb2 as communication_pb2
import logging


# all fields without a time_stamp field is ignored
# the one with higher time_stamp is selected
# note that the object1 and object2 will be modified in this function
# might need to consider using some copy function to prevent this?
def merge_objects(object1, object2):
    if not (dir(object1) == dir(object2)):
        logger = logging.getLogger(__name__)
        logger.warning("blocked attempts to merge protobuf object of different type")
        return None

    for field in dir(object1):
        field_data1 = getattr(object1, field)
        field_data2 = getattr(object2, field)
        if not hasattr(field_data1, 'time_stamp'):
            continue
        if field_data2.time_stamp > field_data1.time_stamp:
            getattr(object1, field).MergeFrom(field_data2)

    return object1


# note that because python PASS BY OBJECT REFERENCE
# changes to the object in the function will be effected on the original object
# we do not need to return anything
def copy_grpc_vec3_to_python(grpc_object, python_object):
    python_object[0] = grpc_object.x
    python_object[1] = grpc_object.y
    python_object[2] = grpc_object.z
    python_object.time_stamp = grpc_object.time_stamp


def copy_python_vec3_to_grpc(grpc_object, python_object):
    grpc_object.x = python_object[0]
    grpc_object.y = python_object[1]
    grpc_object.z = python_object[2]
    grpc_object.time_stamp = python_object.time_stamp
