from abc import ABC, abstractmethod  # Python abstract base class
from PythonComponent.shared.gRPC.containers import *  # this import time is for documentation
'''Abstract class defines the functions of the client side gRPC interface,
 particularly those which are accessible from the GUI'''


class ClientInterface(ABC):
    # setters
    # pass in the things you want to set in the form of container class
    # returns an ErrorStatus enum
    @abstractmethod
    def set_velocity(self, velocity):
        """ Set target velocity

        Args:
            velocity (Velocity): Target velocity. Time stamp must be set

        Returns:
            ErrorStatus: error status if any
        """
        pass

    @abstractmethod
    def set_rotation(self, rotation):
        """ Set target rotation

        Args:
            rotation (Rotation): Target rotation. Time stamp must be set

        Returns:
            ErrorStatus: error status if any
        """
        pass

    @abstractmethod
    def set_light_brightness(self, light_index, light_brightness):
        """ Set target light brightness

        Args:
            light_index (LightInd): The index of the light
            light_brightness (int): Target brightness (0 - 100) in percentage

        Returns:
            ErrorStatus: error status if any
        """
        pass

    @abstractmethod
    def set_pid_value(self, pid_value):
        """Set target light brightness

        Args:
            pid_value (PIDValue): Target PID value. Time stamp must be set

        Returns:
            ErrorStatus: error status if any
        """
        pass

    # getters
    @abstractmethod
    def read_target_velocity(self):
        """Reads latest target velocity sent back from mbed
        Note that if there is no new data from last read, this will return ErrorStatus.DATA_UNAVAILABLE

        Returns:
            Velocity: latest data
        """
        pass

    @abstractmethod
    def read_target_rotation(self):
        """Reads latest target rotation sent back from mbed
        Note that if there is no new data from last read, this will return ErrorStatus.DATA_UNAVAILABLE

        Returns:
            Rotation: latest data
        """
        pass

    @abstractmethod
    def read_light_brightness(self):
        """Reads latest light brightness sent back from mbed
        Note that if there is no new data from last read, this will return ErrorStatus.DATA_UNAVAILABLE

        Returns:
            `list` of `LightBrightness`: latest data
        """
        pass

    @abstractmethod
    def read_imu(self):
        """Reads latest imu reading sent back from mbed
        Note that if there is no new data from last read, this will return ErrorStatus.DATA_UNAVAILABLE

        Returns:
            IMUData: latest data
        """
        pass

    @abstractmethod
    def read_thruster_status(self):
        """Reads latest current thruster speed sent back from mbed
        Note that if there is no new data from last read, this will return ErrorStatus.DATA_UNAVAILABLE

        Returns:
            ThrusterStatus: latest data (not that ThrusterStatus.speed is a list of speeds)
        """
        pass

    @abstractmethod
    def read_button_status(self):
        """Reads latest button status sent back from mbed
        Note that if there is no new data from last read, this will return ErrorStatus.DATA_UNAVAILABLE

        Returns:
            ButtonStatus: latest data (note that ButtonStatus.is_pressed in a list)
        """
        pass

    @abstractmethod
    def read_depth(self):
        """Reads latest current depth sent back from mbed
        Note that if there is no new data from last read, this will return ErrorStatus.DATA_UNAVAILABLE

        Returns:
            DepthData: latest data
        """
        pass

    @abstractmethod
    def read_sonar(self):
        pass

    @abstractmethod
    def read_img(self):
        """Reads latest image frame sent back from mbed
        Note that if there is no new data from last read, this will return ErrorStatus.DATA_UNAVAILABLE

        Returns:
            ImgData: latest data
        """
        pass

    @abstractmethod
    def read_battery_status(self):
        """Reads latest battery status sent back from mbed
        Note that if there is no new data from last read, this will return ErrorStatus.DATA_UNAVAILABLE

        Returns:
            ImgData: latest data
        """
        pass

    # other functions
    @abstractmethod
    def emergency_stop(self):
        pass

    @abstractmethod
    def hardware_reset(self):
        pass
