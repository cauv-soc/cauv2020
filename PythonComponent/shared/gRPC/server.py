import PythonComponent.shared.gRPC.communication_pb2_grpc as communication_pb2_grpc
import logging
import grpc
import concurrent.futures as futures


class Server(object):
    def __init__(self, servicer, port='[::]:50051', max_worker=1):
        logging.info("Starting to prepare server")
        self.server = grpc.server(futures.ThreadPoolExecutor(max_workers=max_worker))
        # do not change the name of these functions, as the code is auto generated
        communication_pb2_grpc.add_grpcCommunicationServicer_to_server(servicer, self.server)
        logging.info('using port: ' + port)
        self.server.add_insecure_port(port)
        logging.info("Finish preparing server")

    def start(self):
        self.server.start()
        logging.info("Server started")
