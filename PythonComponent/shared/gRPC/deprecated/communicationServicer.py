import logging
import pickle

import PythonComponent.shared.gRPC.communication_pb2 as communication_pb2
import PythonComponent.shared.gRPC.communication_pb2_grpc as communication_pb2_grpc
import cv2
import numpy as np
from PythonComponent.shared.gRPC.containers import *
from PythonComponent.shared.utilities.queueUtilities import *


class CommunicationServicer(communication_pb2_grpc.CommunicationServicer):
    def __init__(self):
        # assume that things will be set properly after the constructor
        self.imageStreamingQ = None
        self.groundStationInstructionQ = None
        self.statusUpdateQ = None

        # self.vidCapture = _vidCapture
        pass  # looks like that I do not have to do anything at this moment

    def setImageStreamingQ(self, imageStreamingQ):
        self.imageStreamingQ = imageStreamingQ

    def setGroundStationInstructionQ(self, groundStationInstructionQ):
        self.groundStationInstructionQ = groundStationInstructionQ

    def setStatusUpdateQ(self, statusUpdateQ):
        self.statusUpdateQ = statusUpdateQ

    # for generic RPC, decide what to do based on the class received
    # some of it might be blocking
    # but it should not be blocking by that much
    def genericRPC(self, request, context):
        receivedContainer = pickle.loads(request.data)

        # if an empty container is received, we will fill it up and return it
        if isinstance(receivedContainer, ImgContainer):
            logging.debug('img Requested')
            receivedContainer.rawImg = read_latest(self.imageStreamingQ)
            binaryData = pickle.dumps(receivedContainer)
            returnMsg = communication_pb2.genericPickleMsg(data=binaryData)
            logging.info('Return message packaged')
            return returnMsg
        emptyMsg = communication_pb2.genericPickleMsg()
        return emptyMsg

    def Echo(self, request, context):
        logging.info('Communication server received: ' + request.Msg)
        return request

    def StatusReport(self, request, context):
        logging.info('Status Requested')
        # retrieve the image
        cvImg = read_latest(self.imageStreamingQ)
        str_encode = bytes()
        if not (cvImg is None):
            logging.info('Img Obtained')
            img_encode = cv2.imencode('.jpg', cvImg)
            data_encode = np.array(img_encode[1])
            str_encode = data_encode.tostring()
            # logging.info('Img encoded to string')
        # retrieve the status report from main logic thread
        # nothing as of now
        returnMsg = communication_pb2.Status(encodedImg=str_encode)
        logging.info('Return message packaged')
        return returnMsg

    def statusReport(self, request, context):
        logging.info('Status Requested')
        # retrieve the image
        cvImg = read_latest(self.imageStreamingQ)
        # skip encoding for now
        statusReport = StatusReportContainer()
        # do not really have to care about whether it is None here
        statusReport.imgFrameRaw = cvImg
        binaryData = pickle.dumps(statusReport)
        returnMsg = communication_pb2.genericPickleMsg(data=binaryData)
        logging.info('Return message packaged')
        return returnMsg
        pass

# logging.basicConfig(level=logging.INFO)
# vidCapture = VideoCaptureAsync(src=0, useUSBCam=True)
# vidCapture.start()
# time.sleep(1)
# vidCapture.read()
# communicationServicer = CommunicationServicer()
# communicationServicer.setImageStreamingQ(vidCapture.imageQueueForStreaming)
# myServer = Server(communicationServicer)
# myServer.start()
# # serve()
# time.sleep(100000)
