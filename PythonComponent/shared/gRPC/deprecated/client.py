import PythonComponent.shared.gRPC.communication_pb2 as communication_pb2
import PythonComponent.shared.gRPC.communication_pb2_grpc as communication_pb2_grpc
import logging
import grpc
import queue
import time
import pickle
from PythonComponent.shared.gRPC.containers import *
import threading


# note that the communication channel used in a context manager
# the context manager deals with issues such as closing after exit
# it is probably best not to mess with it
# as such, the current code uses context manager as well
class Client(object):
    def __init__(self, channelName='localhost:50051', updateFreq_ms=20):
        self.channelName = channelName
        self.updateFreq_ms = updateFreq_ms
        self.statusUpdateQueue = queue.Queue()
        self.GSInstructionQueue = queue.Queue()
        self.picklePacked = True
        self.thread = threading.Thread(target=self.update)

    def run(self):
        self.thread.daemon = True
        self.thread.start()

    # to-do:
    # probably merge the two grpc functions into 1 function?
    def update(self):
        logging.info('creating channel@ ' + self.channelName)
        # context manager
        with grpc.insecure_channel(self.channelName) as channel:
            stub = communication_pb2_grpc.CommunicationStub(channel)
            logging.info('channel and stub created')
            placeHolder = communication_pb2.PlaceHolder()
            logging.info("placeHolder message generated, entering loop")
            while True:
                # first get statusUpdate
                returnMsg = stub.statusReport(placeHolder)
                print(type(returnMsg))
                statusReport = pickle.loads(returnMsg.data)
                if not isinstance(statusReport, StatusReportContainer):
                    logging.info('error')
                else:
                    logging.info('placed in queue')
                    self.statusUpdateQueue.put(statusReport)

                # next send in the new instruction if there is any
                # not implemented yet
                time.sleep(self.updateFreq_ms/1000)
