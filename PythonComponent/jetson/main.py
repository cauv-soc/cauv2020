from PythonComponent.shared.gRPC.deprecated.communicationServicer import *
from PythonComponent.shared.utilities.findIP import get_ip
import logging
from PythonComponent.shared.gRPC.server import Server
from PythonComponent.jetson.ImageProc.videoCaptureAsync import VideoCaptureAsync

logging.basicConfig(level=logging.INFO)
# get the ip_address
myIP = get_ip()
port = myIP + ':50051'
print(port)
# start video
vidCapture = VideoCaptureAsync(src=0, useUSBCam=True)
vidCapture.start()
time.sleep(1)
vidCapture.read()
# set up server
communicationServicer = CommunicationServicer()
communicationServicer.setImageStreamingQ(vidCapture.imageQueueForStreaming)
myServer = Server(communicationServicer, port=port)
myServer.start()
# serve()
time.sleep(100000)