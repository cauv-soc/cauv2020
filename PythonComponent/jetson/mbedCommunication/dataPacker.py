from PythonComponent.shared.gRPC.containers import *
import struct


class DataPacker:

    def __init__(self):
        self.NUM_HEADLIGHTS = 4
        self.NUM_MBED_LIGHTS = 3

    # encoding functions

    def encode_target_velocity(self, velocity):
        if type(velocity) == TargetVelocity:
            # convert velocities to a 7 bit unsigned integer
            vx = round(abs(velocity.vx) * 16)
            vy = round(abs(velocity.vy) * 16)
            vz = round(abs(velocity.vz) * 16)

            sign = 0
            sign += 1 if velocity.vx < 0 else 0
            sign += 2 if velocity.vy < 0 else 0
            sign += 4 if velocity.vz < 0 else 0

            return [sign, vx, vy, vz]
        else:
            return None

    def encode_target_rotation(self, rotation):
        if type(rotation) == TargetRotation:

            # yaw and roll in range[-180, 180]
            # pitch in range [-90, 90]
            yaw_int = round(abs(rotation.yaw) / 2)
            pitch_int = round(abs(rotation.roll))
            roll_int = round(abs(rotation.pitch) / 2)

            sign = 0
            sign += 1 if rotation.yaw < 0 else 0
            sign += 2 if rotation.pitch < 0 else 0
            sign += 4 if rotation.roll < 0 else 0

            return [sign, yaw_int, pitch_int, roll_int]
        else:
            return None

    def encode_pid_values(self, pid):
        if type(pid) == PIDValue:
            P = round(abs(pid.p) * 8192.0 / 512.0) + (8192 if (pid.p < 0) else 0)
            I = round(abs(pid.i) * 8192.0 / 512.0) + (8192 if (pid.p < 0) else 0)
            D = round(abs(pid.d) * 8192.0 / 512.0) + (8192 if (pid.p < 0) else 0)

            # each value as a 14 bit integer, packaged MSB first
            return [P // 128, P % 128, I // 128, I % 128, D // 128, D % 128]
        else:
            return None

    def encode_headlight_values(self, light_brightness):
        if type(light_brightness) == TargetLightBrightness:
            b = light_brightness.brightness[:self.NUM_HEADLIGHTS]
            #  if all 4 light values are not given they default to 0
            while len(b) < self.NUM_HEADLIGHTS:
                b.append(0)

            return b

    def encode_mbed_light_values(self, light_brightness):
        if type(light_brightness) == TargetLightBrightness:
            b = light_brightness.brightness[:self.NUM_MBED_LIGHTS]
            #  if all 4 light values are not given they default to 0
            while len(b) < self.NUM_MBED_LIGHTS:
                b.append(0)

            return b

    # decoding functions for data received from mbed

    def decode_current_rotation(self, data):
        yaw_uint = data[0] * 128 + data[1]
        pitch_uint = data[2] * 128 + data[3]
        roll_uint = data[4] * 128 + data[5]

        yaw = (yaw_uint if yaw_uint < 8192 else (8192 - yaw_uint)) / 32.0
        pitch = (pitch_uint if pitch_uint < 8192 else (8192 - pitch_uint)) / 64.0
        roll = (roll_uint if roll_uint < 8192 else (8192 - roll_uint)) / 32.0

        return Rotation(yaw=yaw, pitch=pitch, roll=roll)

    def map_range(self, x, in_range, out_range):
        """Linearly maps a value of x from a range (in_min, in_max)
        to a range (out_min, out_max). Note that the value is
        not constrained to the output range, nor does x need to be
        constrained to the input range"""

        return ((x - in_range[0]) / (in_range[1] - in_range[0])) * (out_range[1] - out_range[0]) + out_range[0]
