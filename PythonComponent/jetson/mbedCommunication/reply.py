class Reply:
    END_MESSAGE = 0
    ECHO = 1
    IMU_ROTATION_READING = 2
    TARGET_ORIENTATION = 3
    IMU_ANGULAR_SPEED = 4
    IMU_LINEAR_ACC = 5
    RAW_SPEED = 6
    INTENDED_SPEED = 7
    BTN_STATUS = 8

    def __init__(self):
        pass

    @staticmethod
    def SizeMapper(instruction):
        if instruction == Reply.END_MESSAGE:
            return 0
        if instruction == Reply.ECHO:
            return 1
        if instruction == Reply.IMU_ROTATION_READING:
            return 6
        if instruction == Reply.TARGET_ORIENTATION:
            return 4
        if instruction == Reply.IMU_ANGULAR_SPEED:
            return 4
        if instruction == Reply.IMU_LINEAR_ACC:
            return 4
        if instruction == Reply.RAW_SPEED:
            return 8
        if instruction == Reply.INTENDED_SPEED:
            return 8
        if instruction == Reply.BTN_STATUS:
            return 1

