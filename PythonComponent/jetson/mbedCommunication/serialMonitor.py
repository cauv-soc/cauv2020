import serial
import serial.tools.list_ports
import sys
import serial

# initialization: scan the ports to find mbed
ports = list(serial.tools.list_ports.comports())
serial_port_id = None
for p in ports:
    print(p)
    if 'STLink' in p[1]:  # this is the identifier used to locate mbed
        serial_port_id = p[0]
        print('mbed found')
        break

if serial_port_id is None:
    print('mbed not found')
    sys.exit()

# now start the serial port
serialPort = serial.Serial(
    port=serial_port_id,
    baudrate=115200,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS,
    timeout=0.1
)

print('port open')
while True:
    while serialPort.inWaiting() > 0:
        c = serialPort.read()  # this reads one byte by default
        print(c.decode("utf-8"), end='')  # the decode command is needed to get the correct thing
        # print(ord(c), end=' ')
        # print(c.decode("utf-8"))



