import struct
import sys
import time

import serial
import serial.tools.list_ports


# issue: how to narrow down the scope for the except so that it does not catch other error
# may try: except serial.SerialException:


class SerialInterface(object):
    def __init__(self, identifier='USB'):
        self.identifier = identifier
        self.mbedAvailable = False
        self.ser = None
        self.buffer = bytes([])

        self.init_serial()
        if not self.mbedAvailable:
            self.handle_serial_exception()
        # while not self.mbedAvailable:
        #     time.sleep(500)
        #     self.initSerial()

    def init_serial(self):
        self.ser = None
        self.mbedAvailable = False  # actually this function is not supposed to be called with mbed available
        serial_port_mbed = None

        ports = list(serial.tools.list_ports.comports())
        for p in ports:
            print(p)
            if self.identifier in p[1]:  # this is the identifier used to locate mbed
                serial_port_mbed = p[0]
                print('mbed found, identifier used ', end='')
                print(self.identifier)
                self.mbedAvailable = True

        # initialize the serial port when a Mbed is found among USB devices
        if self.mbedAvailable:
            self.ser = serial.Serial(
                port=serial_port_mbed,
                baudrate=115200,
                parity=serial.PARITY_NONE,
                stopbits=serial.STOPBITS_ONE,
                bytesize=serial.EIGHTBITS,
                timeout=0.1
            )
            print('port open')
        else:
            print('Mbed Not Found')

    def handle_serial_exception(self):
        print('Mbed connection lost')
        self.mbedAvailable = False
        counter = 0
        while not self.mbedAvailable:
            time.sleep(1)
            counter = counter + 1
            print('Trying to re-establish connection. Try %d', counter, end='     ')
            self.init_serial()
            if self.mbedAvailable:
                print('connection re-established')
                break
            else:
                print('failed')
                if counter > 20:
                    print('We give up')
                    sys.exit()

    def add(self, number):
        self.buffer = self.buffer + struct.pack(b'B', number)

    def transmit(self):
        try:
            self.ser.write(self.buffer)
            self.buffer = self.buffer = bytes([])
        except serial.SerialException:
            print("error in transmit")
            self.handle_serial_exception()

    def available(self):
        try:
            if self.ser.inWaiting() > 0:
                return True
            else:
                return False
        except serial.SerialException:
            print("error in reading")
            self.handle_serial_exception()
            return False

    # this is a blocking read
    def read(self):
        try:
            while self.ser.inWaiting <= 0:
                pass
            self.ser.read()
        except serial.SerialException:
            print("error in reading")
            self.handle_serial_exception()
            return self.read()  # not exactly sure if this is a good practice at all...

    # debug functions
    def debug_read_number(self):
        try:
            while self.ser.inWaiting() > 0:
                print(ord(self.ser.read()))
        except serial.SerialException:
            print("error in reading")
            self.handle_serial_exception()

    def debug_read_str(self):
        try:
            while self.ser.inWaiting() > 0:
                c = self.ser.read()
                try:
                    print(c.decode("utf-8"), end='')
                except UnicodeDecodeError:
                    print("\nnot string")
        except serial.SerialException:
            print("error in reading")
            self.handle_serial_exception()
