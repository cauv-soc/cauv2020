import pathlib
import sys
# retrieve the root folder of the project
# note that this is kind of hard coded...
currFilePath = pathlib.Path(__file__)
rootDirPath = currFilePath.parent.parent.parent.parent.parent
cRelativePath = "/mbedComponent/communication/"
pyRelativePath = "/PythonComponent/jetson/mbedCommunication/"

fileNameList = ["instruction", "reply"]

for fileName in fileNameList:
    instructionInfo = open(fileName + ".csv")
    line = instructionInfo.readline()

    instructionName = []
    expectedLen = []
    lineCnt = 0
    maxExpectedLen = 0

    while line:
        line = line.strip()
        if len(line) == 0:
            line = instructionInfo.readline()
            continue
        if line[0] == '#':
            line = instructionInfo.readline()
            continue
        lineCnt = lineCnt + 1
        separated = line.split(",")
        if not (len(separated) == 3):
            print("ERROR at line", lineCnt, " 3 entries expected")
            sys.exit()
        instructionName.append(separated[0].strip())
        expectedLen.append(separated[1].strip())
        if int(separated[1].strip()) > maxExpectedLen:
            maxExpectedLen = int(separated[1].strip())
        line = instructionInfo.readline()

    instructionInfo.close()
    print(instructionName)
    print(expectedLen)
    print(maxExpectedLen)

    fileName = fileName
    # now create the c++ file
    h_file = open(str(rootDirPath) + cRelativePath + fileName + ".h", "w+")

    defSymbol = fileName.upper() + "_H"
    endl = "\n"
    indent = "    "
    enumName = fileName
    enumName = enumName[0].upper() + enumName[1:]

    h_file.write("#ifndef " + defSymbol + endl)
    h_file.write("#define " + defSymbol + endl)
    h_file.write(endl)
    h_file.write("#define MAX_DATA_FIELD_LENGTH " + str(maxExpectedLen) + endl)
    h_file.write(endl)
    h_file.write("enum class " + enumName + endl)
    h_file.write("{" + endl)
    for i in range(lineCnt):
        if not (i == lineCnt - 1):
            h_file.write(instructionName[i] + "," + endl)
        else:
            h_file.write(instructionName[i] + endl)

    h_file.write("};" + endl)
    h_file.write(endl)
    h_file.write("int " + fileName + "SizeMapper(" + enumName + " instruction);" + endl)
    h_file.write(endl)
    h_file.write("#endif")
    h_file.close()

    cpp_file = open(str(rootDirPath) + cRelativePath + fileName + ".cpp", "w+")
    cpp_file.write("#include \"" + fileName + ".h\"" + endl)
    cpp_file.write(endl)
    cpp_file.write("int " + fileName + "SizeMapper(" + enumName + " instruction)" + endl)
    cpp_file.write("{" + endl)
    cpp_file.write(indent + "switch(instruction)" + endl)
    cpp_file.write(indent + "{" + endl)

    for i in range(lineCnt):
        cpp_file.write(indent + indent + "case " + enumName + "::" + instructionName[i] + ":" + endl)
        cpp_file.write(indent + indent + indent + "return " + expectedLen[i] + ";" + endl)
    cpp_file.write(indent + "}" + endl)
    cpp_file.write("}" + endl)
    cpp_file.close()

    py_file = open(str(rootDirPath) + pyRelativePath + fileName + ".py", "w+")
    py_file.write('class ' + enumName + ':' + endl)

    for i in range(lineCnt):
        py_file.write(indent + instructionName[i] + ' = ' + str(i) + endl)
    py_file.write(endl)

    py_file.write(indent + 'def __init__(self):' + endl)
    py_file.write(indent + indent + 'pass' + endl)
    py_file.write(endl)

    py_file.write(indent + '@staticmethod' + endl)
    py_file.write(indent + 'def ' + 'SizeMapper(instruction):' + endl)
    for i in range(lineCnt):
        py_file.write(indent + indent + 'if instruction == ' + enumName + '.' + instructionName[i] + ":" + endl)
        py_file.write(indent + indent + indent + 'return ' + str(expectedLen[i]) + endl)
    py_file.write(endl)
    py_file.close()
