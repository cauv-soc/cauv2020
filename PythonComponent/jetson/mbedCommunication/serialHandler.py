import logging
import queue
import struct
import sys
import threading
import time
from datetime import datetime

import serial
import serial.tools.list_ports
from PythonComponent.jetson.mbedCommunication.reply import Reply
from PythonComponent.jetson.mbedCommunication.instruction import Instruction


# note: might need to remove the datetime stuff later

# https://realpython.com/python-print/#thread-safe-printing
# https://realpython.com/intro-to-python-threading/
# https://docs.python.org/3/library/logging.html
# https://pyserial.readthedocs.io/en/latest/pyserial_api.html
# https://docs.python.org/2/library/queue.html#Queue.Empty


# maybe there is not a point having a separate serial interface class, the two might be merged together
# note the for pySerial, write and read are both blocking by default
# print is not thread safe, but logging is
# should use logging instead of printing
class SerialHandler(object):
    def __init__(self, identifier='USB', is_threaded=True):  # the USB port is linked via FDTI to UART.
        self.identifier = identifier
        self.mbedAvailable = False
        self.ser = None
        self.buffer = bytes([])
        self.serialRecoveryLock = threading.Lock()  # used to ensure that only one thread is trying to recover serial
        self.init_serial()
        if not self.mbedAvailable:
            self.handle_serial_exception()

        # it is assumed that a string will be put into queue
        self.readQueue = queue.Queue()
        self.writeQueue = queue.Queue()
        self.waitTime = 0.005  # don't make some delay when there is nothing to read or write

        if is_threaded:  # not threaded is only used for debugReadStr and debugReadNumber function
            readPortThread = threading.Thread(target=self.read_port, daemon=True)
            readPortThread.start()
            writeToPortThread = threading.Thread(target=self.write_to_port, daemon=True)
            writeToPortThread.start()
        else:
            logging.warning('SerialHandler:: non-threading debug reading mode is used!')

    def init_serial(self):
        self.ser = None
        self.mbedAvailable = False  # actually this function is not supposed to be called with mbed available
        serialPortMbed = None

        ports = list(serial.tools.list_ports.comports())
        for p in ports:
            logging.info(str(p))
            if self.identifier in p[1]:  # this is the identifier used to locate mbed
                serialPortMbed = p[0]
                logging.info('mbed found, identifier used %s', self.identifier)
                self.mbedAvailable = True

        # initialize the serial port when a Mbed is found among USB devices
        if self.mbedAvailable:
            self.ser = serial.Serial(
                port=serialPortMbed,
                baudrate=115200,
                parity=serial.PARITY_NONE,
                stopbits=serial.STOPBITS_ONE,
                bytesize=serial.EIGHTBITS,
                timeout=None  # 0.1  note that this only sets a read time out, now it is blocking
            )
            logging.info('port open')
        else:
            logging.info('Mbed Not Found')

    # must ensure that mbed available is set to false before this function is called
    def handle_serial_exception(self, stage=''):
        # context manager is recommended to ensure that the lock is always released after use
        with self.serialRecoveryLock:
            logging.warning('Mbed connection lost in ' + stage)
            if self.mbedAvailable:
                return
            counter = 0
            while not self.mbedAvailable:
                time.sleep(1)
                counter = counter + 1
                logging.info('Trying to re-establish connection. Try %d', counter)
                self.init_serial()
                if self.mbedAvailable:
                    logging.warning('connection re-established')
                    break
                else:
                    logging.info('failed')
                    if counter > 20:
                        logging.warning('We give up')
                        sys.exit()  # no idea what this will cause...

    def send_msg(self, instruction=0, data=None):
        funcName = 'sendMsg:: '
        logging.debug(funcName + 'sending message %d', instruction)
        logging.debug(funcName + 'data: ' + str(data))
        if data is None:
            data = []
        expectedLen = Instruction.SizeMapper(instruction)

        # do some checking first to make sure things are sendable
        if not(len(data) == expectedLen):
            logging.warning(funcName + 'error in sending message %d, length mismatch', instruction)
            sys.exit()
        for i in range(len(data)):
            if not(isinstance(data[i], int)):
                logging.warning(funcName + 'error in sending message %d, data not int', instruction)
            if (data[i] < 0) or (data[i] > 127):
                logging.warning(funcName + 'error in sending message %d, data not in range[0, 127]', instruction)

        sendingBuf = [instruction + 128]
        sendingBuf = sendingBuf + data  # join the two together
        sendingBuf = sendingBuf + [Instruction.END_MESSAGE + 128]
        self.writeQueue.put(sendingBuf)
        now = datetime.now()
        logging.debug(funcName + 'data added to queue' + now.strftime("%H:%M:%S:%f"))

    def receive_msg(self):
        funcName = 'receiveMsg:: '
        try:
            return self.readQueue.get_nowait()
        except queue.Empty:
            logging.debug(funcName + 'queue empty')
            return None

    def read_port(self):
        funcName = 'readPort:: '
        while True:
            # scanning for reply
            logging.log(5, funcName + 'scanning for Reply')  # use a low level logging
            incoming = 0
            while True:
                try:
                    incoming = ord(self.ser.read())
                except serial.SerialException:
                    self.mbedAvailable = False
                    self.handle_serial_exception('reading')
                except TypeError:
                    logging.debug('throw TypeError, likely to be self.ser.read() giving empty string')
                    logging.debug('Now interpreted as mbed connection lost')
                    self.mbedAvailable = False
                    self.handle_serial_exception('reading')
                logging.log(5, funcName + 'incoming %d', incoming)
                # check if it is a reply
                if incoming > 128:
                    logging.log(5, funcName + 'reply found')
                    break
            reply = incoming - 128

            # scanning for data
            data = []
            logging.log(5, funcName + 'scanning for data')  # use a low level logging
            while True:
                try:
                    incoming = ord(self.ser.read())
                except serial.SerialException:
                    self.mbedAvailable = False
                    self.handle_serial_exception('reading')
                logging.log(5, funcName + 'incoming %d', incoming)
                # if it is an END_MESSAGE
                if incoming == 128:
                    if len(data) == Reply.SizeMapper(reply):
                        self.readQueue.put(ReplyContainer(reply, data))
                        now = datetime.now()
                        logging.debug(funcName + 'frame complete:' + now.strftime("%H:%M:%S:%f"))
                        logging.debug(funcName + 'reply: ' + str(reply) + ', data: ' + str(data))
                    else:
                        logging.warning(funcName + 'data size mismatch')
                        logging.debug(funcName + 'data: ' + str(data))
                    break
                # if an reply is hit instead
                if incoming > 128:
                    logging.warning(funcName + 'missing end message, frame discarded')
                    logging.debug(funcName + 'reply: ' + str(reply) + ', data: ' + str(data))
                    break
                # if it is just a normal data pt
                if incoming < 128:
                    data.append(incoming)
                    if len(data) > Reply.SizeMapper(reply):
                        logging.warning(funcName + 'data too long, something is very wrong, frame discarded')
                        logging.debug(funcName + 'reply: ' + str(reply) + ', data: ' + str(data))
                        break

    def write_to_port(self):
        funcName = 'writeToPort:: '
        while True:
            try:
                newInstruction = self.writeQueue.get_nowait()
                # do the packing
                self.buffer = bytes([])
                for i in range(len(newInstruction)):
                    self.buffer = self.buffer + struct.pack(b'B', newInstruction[i])
                # use a while loop to make sure that the thing is always written properly
                while not (self.buffer == bytes([])):
                    try:
                        self.ser.write(self.buffer)
                        self.buffer = bytes([])
                        logging.debug(funcName + 'instruction successfully written')
                        logging.debug(funcName + 'data stream: ' + str(newInstruction))
                    except serial.SerialException:
                        self.mbedAvailable = False
                        self.handle_serial_exception('transmit')
            # do some waiting if there is nothing to retrieve from the queue
            except queue.Empty:
                time.sleep(self.waitTime)

    # some debug functions that are not threaded
    # just try not to use them at all
    def debug_read_number(self):
        try:
            while self.ser.inWaiting() > 0:
                print(ord(self.ser.read()))
        except serial.SerialException:
            self.mbedAvailable = False
            self.handle_serial_exception('reading')

    def debug_read_str(self):
        try:
            while self.ser.inWaiting() > 0:
                c = self.ser.read()
                try:
                    print(c.decode("utf-8"), end='')
                except UnicodeDecodeError:
                    print("\nnot string")
        except serial.SerialException:
            print("error in reading")
            self.mbedAvailable = False
            self.handle_serial_exception('reading')


class ReplyContainer(object):
    def __init__(self, reply, data):
        self.reply = reply
        self.data = data
