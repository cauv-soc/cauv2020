import logging
import time
import sys

from PythonComponent.jetson.mbedCommunication.serialHandler import *
from PythonComponent.shared.gRPC.containers import *
from PythonComponent.jetson.mbedCommunication.dataPacker import DataPacker

# takes about 10 ms for the echo to finish
# remember to take out the debug delay time on mbed
# otherwise, a ~5 s delay will be seen because of that
if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)

    serialHandler = SerialHandler()
    serialEncoder = DataPacker()

    rot = TargetRotation(roll=30.0, pitch=25.0, yaw=147.0)
    velocity = TargetVelocity(vx=1.0, vy=3.2, vz=0.3)
    pid = PIDValue(p=10.3, i=52.2, d=71.8)
    mbed_light_on = TargetLightBrightness(brightness=[127,0,127])
    mbed_light_off = TargetLightBrightness(brightness=[0,127,0])

    rotation_msg = serialEncoder.encode_target_rotation(rot)
    velocity_msg = serialEncoder.encode_target_velocity(velocity)
    pid_msg = serialEncoder.encode_pid_values(pid)
    mbed_light_on_msg = serialEncoder.encode_mbed_light_values(mbed_light_on)
    mbed_light_off_msg = serialEncoder.encode_mbed_light_values(mbed_light_off)

    while True:
        serialHandler.send_msg(Instruction.SET_TARGET_ROTATION, rotation_msg)
        time.sleep(1)
        serialHandler.send_msg(Instruction.SET_TARGET_VELOCITY, velocity_msg)
        time.sleep(1)
        serialHandler.send_msg(Instruction.SET_PID_VALUE, pid_msg)
        time.sleep(1)

        while True:
            # read all available serial replies from the mbed
            msg = serialHandler.receive_msg()
            if msg:
                if msg.reply == Reply.IMU_ROTATION_READING:
                    logging.info(serialEncoder.decode_current_rotation(msg.data))
            else:
                break

        # lights on mbed should flash
        serialHandler.send_msg(Instruction.SET_MBED_LIGHTS, mbed_light_on_msg)
        time.sleep(3)
        serialHandler.send_msg(Instruction.SET_MBED_LIGHTS, mbed_light_off_msg)
        time.sleep(1)
