from PythonComponent.jetson.ImageProc.videoCaptureAsync import VideoCaptureAsync
import cv2
import time
import logging

logging.basicConfig(level=logging.DEBUG)
vid_capture = VideoCaptureAsync(src=0, useUSBCam=True)
vid_capture.start()
time.sleep(1)
t0 = time.time()
tMax = 10  # let the program run for 60s
cnt = 0

while time.time() - t0 < tMax:
    new_frame = vid_capture.read()
    cnt = cnt + 1
    cv2.imwrite('test' + str(cnt) + '.jpg', new_frame)

vid_capture.stop()
print('fps:' + str(cnt/tMax))

