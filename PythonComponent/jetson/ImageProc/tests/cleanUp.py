import os

# this basically removes all .jpg file saved under the current directory
file_list = [f for f in os.listdir('.') if f.endswith(".jpg")]
print(file_list)
for file in file_list:
    os.remove(file)
