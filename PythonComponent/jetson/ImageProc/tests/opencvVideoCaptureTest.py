from PythonComponent.jetson.ImageProc.opencvVideoCapture import OpencvVideoCapture
import cv2
import time
import logging
from queue import Queue, Empty

logging.basicConfig(level=logging.DEBUG)
img_queue = Queue()
streaming_queues = [img_queue]
vid_capture = OpencvVideoCapture(src=0, use_usb_cam=True, streaming_queue=streaming_queues)
vid_capture.start()
time.sleep(1)
t0 = time.time()
tMax = 10  # let the program run for 10s
cnt = 0

while time.time() - t0 < tMax:
    try:
        new_img_data = img_queue.get_nowait()
        new_frame = new_img_data.raw
        cnt = cnt + 1
        cv2.imwrite('test' + str(cnt) + '.jpg', new_frame)
    except Empty:
        pass

vid_capture.stop()
print('fps:' + str(cnt/tMax))

