from threading import Thread, Lock
import cv2
import logging
import sys
import time
from queue import Queue, Empty
from PythonComponent.shared.gRPC.containers import ImgData
from PythonComponent.shared.utilities.timeStamp import curr_time


class OpencvVideoCapture(object):
    """ Camera control and image streaming class

    The class controls the camera, and images are dump to every streaming queue.
    The images are packed in ImgData, and they are accessed through the queue provided

    Attributes:
        max_queue_size (int): the max number of images (default 10) kept in the queue
    """

    def __init__(self, src=0, use_usb_cam=True, streaming_queue=None):
        """
        Initialize the video capture source
        The function checks if items in the streaming_queue provided is a queue.Queue.
        Args:
            src (int): Camera index for cv2.VideoCapture
            use_usb_cam (bool): Set to true(default) if usb_cam is used.
            streaming_queue (list of queue.Queue): A list of streaming queues
        """

        logger = logging.getLogger(__name__)
        self.stopped = False  # used to stop the thread if necessary
        self.max_queue_size = 10

        # check the queue provided
        self.streaming_queue = streaming_queue
        for item in self.streaming_queue:
            if not isinstance(item, Queue):
                logger.critical("Image streaming queue not a queue: " + str(type(item)))
                sys.exit()
        # initialize the camera
        if not use_usb_cam:
            logger.critical('Non-USB camera not supported, exiting')
            sys.exit()
        else:
            self.stream = cv2.VideoCapture(src)
            logger.info('USB camera' + str(src) + 'initialized')
            (_, self.frame) = self.stream.read()
            self.grabbed = True

    def update(self):
        """
        Continuously grab new frames and and dump to every streaming queue.
        The frames are raw frames and are packed in ImgData.
        """

        logger = logging.getLogger(__name__)
        logger.info('video recording thread started')
        while not self.stopped:
            (_, self.frame) = self.stream.read()
            self.grabbed = True
            img_data = ImgData()
            img_data.time_stamp = curr_time()
            img_data.raw = self.frame
            for q in self.streaming_queue:
                # Check if things are accumulating in the queue.
                # Remove something if that is the case.
                if q.qsize() > self.max_queue_size:
                    try:
                        q.get_nowait()
                    except Empty:
                        pass
                q.put(img_data)
        # Print something to indicate the termination of video thread
        logger.info('video recording thread ended')

    def start(self):
        """
        Starts image capture. If this function is not called, there will be no
        frames in the streaming queues.
        """
        videoThread = Thread(target=self.update)
        videoThread.daemon = True
        self.stopped = False
        videoThread.start()

    def stop(self):
        """
        Stop the thread from dumping image into streaming queues.
        As of now, it does not releases the camera
        """
        self.stopped = True

