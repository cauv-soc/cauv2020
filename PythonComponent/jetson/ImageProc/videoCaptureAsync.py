from threading import Thread, Lock
import cv2
import logging
import sys
import time
from queue import Queue, Empty


class VideoCaptureAsync:
    def __init__(self, src=0, useUSBCam=True, streamingEnabled=True):
        logger = logging.getLogger(__name__)
        self.stopped = False  # used to stop the thread if necessary
        self.frame = None  # used to store the frame
        self.imageQueueForComputing = Queue()
        self.streamingEnabled = streamingEnabled
        if self.streamingEnabled:
            self.imageQueueForStreaming = Queue()

        if not useUSBCam:
            logger.critical('Non-USB camera not supported, exiting')
            sys.exit()
        else:
            self.stream = cv2.VideoCapture(src)
            logger.info('USB camera' + str(src) + 'initialized')
            (_, self.frame) = self.stream.read()
            self.grabbed = True
        self.t0 = time.time()

    def update(self):
        logger = logging.getLogger(__name__)
        logger.info('video recording thread started')
        while not self.stopped:
            # logging.debug(funcName + str(time.time() - self.t0))
            (_, self.frame) = self.stream.read()
            self.grabbed = True
            self.imageQueueForComputing.put(self.frame)
            if self.streamingEnabled:
                self.imageQueueForStreaming.put(self.frame)
            # logging.debug(funcName + str(time.time() - self.t0))
        logger.info('video recording thread ended')

    def start(self):
        videoThread = Thread(target=self.update)
        videoThread.daemon = True
        self.stopped = False
        videoThread.start()

    def stop(self):
        self.stopped = True

    def streamingRead(self):
        return self.readLatestFrame(self.imageQueueForStreaming)

    def computingRead(self):
        return self.readLatestFrame(self.imageQueueForComputing)

    @staticmethod
    def readLatestFrame(queueToReadFrom):
        latestFrame = None
        while True:
            try:
                latestFrame = queueToReadFrom.get_nowait()
            except Empty:
                break
        return latestFrame

    # these functions should not be used in new code
    # need to refactor old code to switch to newer functions
    def readNoWait(self):
        try:
            return self.imageQueueForComputing.get_nowait()
        except Empty:
            return None
        # logging.critical('readNoWait not implemented yet')
        # sys.exit()

    def available(self):
        logger = logging.getLogger(__name__)
        logger.critical('available not implemented yet')
        sys.exit()

    def read(self):
        return self.imageQueueForComputing.get()

    # this clears the queue
    def flush(self):
        while True:
            try:
                self.imageQueueForComputing.get_nowait()
            except Empty:
                break

