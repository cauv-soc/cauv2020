from PythonComponent.jetson.deprecated.WebBasedCommunication import app
import flask
import logging
import time
from PythonComponent.jetson.ImageProc.videoCaptureAsync import VideoCaptureAsync
import cv2


# Note that flask searches for template under a folder called templates
# under root directory. Must make the folder!

# roughly works for now. vidCapture can be handled more nicely of course


def generate():
    # grab global references to the output frame and lock variables
    logging.basicConfig(level=logging.DEBUG)
    vidCapture = VideoCaptureAsync(src=0, useUSBCam=True)
    vidCapture.start()
    time.sleep(1)
    print('this is only run once!')
    # loop over frames from the output stream
    while True:
        # wait until the lock is acquired
        outputFrame = vidCapture.read()
        (flag, encodedImage) = cv2.imencode(".jpg", outputFrame)
        if not flag:
            continue

        yield (b'--frame\r\n' b'Content-Type: image/jpeg\r\n\r\n' +
               bytearray(encodedImage) + b'\r\n')


@app.route('/')
@app.route('/index')
def index():
    return flask.Response(generate(),
                          mimetype="multipart/x-mixed-replace; boundary=frame")
    # return flask.render_template('template.html')
# return "Hello, World!"
