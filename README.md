# embeddedController

## Block diagram

![Block diagram](imgs/block_diagram.png "Block diagram")

## Introduction
This repo houses both the python (pc end) and the C++ (mbed end) code.
They are placed in their respective folders.
## To-to
*   Complete comPinMap and update the rest of the code for simplification
*   There may be point in improving the logging thing a bit.
    Basically get nicer format with color and so on. (Low priority)
*   Update the code on the mbed side to support some of the new features
    added on the jetson side
*   Continue implementing things in communication code
*   Video-streaming for grpc
*   Latency test
*   Physical model and control algorithm
## Some cross-check that needed to be done
*   Check comPinMap to make sure that everything is keyed in correctly

## Have a look at Wiki as well!