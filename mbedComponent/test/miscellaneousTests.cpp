#include "mbed.h"
#include "myDebug.h"
#include "globalTimer.h"
#include "debouncedBtn.h"

void userLEDTest()
{
    DigitalOut led1(LED1);
    DigitalOut led2(LED2);
    DigitalOut led3(LED3);
    led1 = 0;
    led2 = 0;
    led3 = 0;

    while(1)
    {
        led1 = 1;
        thread_sleep_for(1000);
        led2 = 1;
        thread_sleep_for(1000);
        led3 = 1;
        thread_sleep_for(1000);
        led1 = 0;
        thread_sleep_for(1000);
        led2 = 0;
        thread_sleep_for(1000);
        led3 = 0;
        thread_sleep_for(1000);     
    }
}

void userButtonTest()
{
    DigitalOut led1(LED1);
    DigitalIn button(USER_BUTTON);
    while(1)
        led1 = button.read();
}

void timerTest()
{
    while(1)
    {
        int time_ms = millis();
        myDebug("time in ms: %d \r\n", time_ms);
        uint64_t time_us = microseconds();
        myDebug("time in microseconds %d\r\n", (int)time_us); // force it to an int so that it can print...
        float time_s = seconds();
        myDebug("time in seconds %f\r\n", time_s);
        thread_sleep_for(5000);
    }
}

void DebouncedBtnSimpleTest()
{
    DebouncedBtn btn(USER_BUTTON);
    DigitalOut led1(LED1);
    DigitalOut led2(LED2);
    DigitalOut led3(LED3);
    led1 = 0;
    led2 = 0;
    led3 = 0;

    while(1)
    {
        while (btn.read() == 0)
            thread_sleep_for(1);
        led1 = 1;
        while(btn.read() == 1)
            thread_sleep_for(1);
        led1 = 0;
        while (btn.read() == 0)
            thread_sleep_for(1);
        led2 = 1;
        while(btn.read() == 1)
            thread_sleep_for(1);
        led2 = 0;
        while (btn.read() == 0)
            thread_sleep_for(1);
        led3 = 1;
        while(btn.read() == 1)
            thread_sleep_for(1);
        led3 = 0;
    }
}

void DebouncedBtnInterruptTest()
{
    DebouncedBtn btn(USER_BUTTON);
    btn.setDebounceMode(DebounceMode::ListenOnInterrupt);
    DigitalOut led1(LED1);
    DigitalOut led2(LED2);
    DigitalOut led3(LED3);
    led1 = 0;
    led2 = 0;
    led3 = 0;

    while(1)
    {
        while (btn.read() == 0)
            thread_sleep_for(1);
        led1 = 1;
        while(btn.read() == 1)
            thread_sleep_for(1);
        led1 = 0;
        while (btn.read() == 0)
            thread_sleep_for(1);
        led2 = 1;
        while(btn.read() == 1)
            thread_sleep_for(1);
        led2 = 0;
        while (btn.read() == 0)
            thread_sleep_for(1);
        led3 = 1;
        while(btn.read() == 1)
            thread_sleep_for(1);
        led3 = 0;
    }
}

void DebouncedBtnSwitchModeTest()
{
    DebouncedBtn btn(USER_BUTTON);
    DigitalOut led1(LED1);
    DigitalOut led2(LED2);
    DigitalOut led3(LED3);
    led1 = 0;
    led2 = 0;
    led3 = 0;
    bool state = true;
    while(1)
    {   
        if (state)
            btn.setDebounceMode(DebounceMode::ListenOnInterrupt);
        else
            btn.setDebounceMode(DebounceMode::Simple);
        state = !state;
        
        while (btn.read() == 0)
            thread_sleep_for(1);
        led1 = 1;
        while(btn.read() == 1)
            thread_sleep_for(1);
        led1 = 0;
        while (btn.read() == 0)
            thread_sleep_for(1);
        led2 = 1;
        while(btn.read() == 1)
            thread_sleep_for(1);
        led2 = 0;
        while (btn.read() == 0)
            thread_sleep_for(1);
        led3 = 1;
        while(btn.read() == 1)
            thread_sleep_for(1);
        led3 = 0;
    }
}