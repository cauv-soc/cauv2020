#ifndef COMMUNICATIONTEST_H
#define COMMUNICATIONTEST_H

#include "mbed.h"
#include "myDebug.h"

// passed
void myDebugTest();

// passed, run with mbedReceiveTest.py
void mbedReceiveTest();

// passed, run with mbedReceiveTest.py
void mbedReceiveEchoTest();

void pcReceiveTest();

void pcSendReceiveTest();

void mbedReceiveCommandsTest();

#endif