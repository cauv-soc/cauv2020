#ifndef MISCELLANEOUS_TEST_H
#define MISCELLANEOUS_TEST_H

#include "mbed.h"

void userLEDTest();

void userButtonTest();

void timerTest();

void DebouncedBtnSimpleTest();

void DebouncedBtnInterruptTest();

void DebouncedBtnSwitchModeTest();

#endif