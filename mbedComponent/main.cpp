#include "mbed.h"
#include "serialHandler.h"
#include "platform/mbed_thread.h"
#include "communicationTests.h"
#include "miscellaneousTests.h"

int main()
{
    //myDebugTest();
    //mbedReceiveTest();
    //mbedReceiveEchoTest();
    //pcReceiveTest();
    //pcSendReceiveTest();
    //userLEDTest();
    //userButtonTest();
    //timerTest();
    mbedReceiveCommandsTest();
    //DebouncedBtnSimpleTest();
    //DebouncedBtnInterruptTest();
    //DebouncedBtnSwitchModeTest();
    return 0;
}

// // main() runs in its own thread in the OS
// UARTSerial pcDebugPort(USBTX, USBRX, 115200);
// UARTSerial pcCom(PB_9, PB_8, 115200); // serial port 4
// int main()
// {
//     pcDebugPort.set_baud(115200);
//     pcDebugPort.set_blocking(false);
//     pcCom.set_baud(115200);
//     pcCom.set_blocking(false);
//     SerialHandler serialHandler(&pcCom, &pcDebugPort);
//     InstructionContainer newInstruction;

//     // just to let it know that it is alive
//     for(int i = 0; i < 10; i++) 
//     {
//         char buf[100];
//         sprintf(buf,"HelloWorld\r\n");
//         pcDebugPort.write(buf, strlen(buf));
//         pcCom.write(buf, strlen(buf));
//         // thread_sleep_for(1000);
//     }
//     // inside the main loop
//     // read all sensor information (going to take quite some time)
//     // check for any incoming message (extremely fast)
//     // update the driving output (extremely fast)
//     // update the jetson server about the information (returns fast, but takes time)
//     while (true)
//     {
//         // read sensors
//         // check for incomming msg
//         while(serialHandler.scanPort(&newInstruction) == PortScanResult::FRAME_READY)
//         {
//             // handle it accordingly
//             if (newInstruction.instruction == Instruction::ECHO)
//                 serialHandler.reply(Reply::ECHO, newInstruction.data_field);
//         }
//     }
// }