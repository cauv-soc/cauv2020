#ifndef DEBOUNCED_BTN_H
#define DEBOUNCED_BTN_H
#include "mbed.h"

enum class DebounceMode
{
    // not always reliable
    Simple,
    // more reliable, but probably more overhead
    // expected behavior: return 0 if unsure i.e. btn now bouncing, otherwise, return curr state
    ListenOnInterrupt
};

class DebouncedBtn
{
public:
    DebouncedBtn(PinName _btnPinName);

    void setDebounceMode(DebounceMode _mode);

    void setDebounceTime(int time_ms);

    int read();

    void interruptCallBack();

private:
    DigitalIn* btnPin = NULL;
    InterruptIn* btnPinI = NULL;
    DebounceMode mode;
    PinName btnPinName;
    int debounceTime;
    int lastState;
    int btnState;
    int lastTime;
    int lastChanged;
};
#endif