#include "debouncedBtn.h"
#include "myDebug.h"
#include "globalTimer.h"

DebouncedBtn::DebouncedBtn(PinName _btnPinName)
{
    btnPinName = _btnPinName;
    // by default, set the debounce mode to Simple
    setDebounceMode(DebounceMode::Simple);
    // set the default debounce time to 50 ms
    setDebounceTime(50);
}

void DebouncedBtn::setDebounceMode(DebounceMode _mode)
{
    mode = _mode;
    if (mode == DebounceMode::Simple)
    {   
        if (btnPinI != NULL)
            delete btnPinI;
        btnPin = new DigitalIn(btnPinName);
        lastState = btnPin->read();
        btnState = lastState;
        lastTime = millis();
    }
    if (mode == DebounceMode::ListenOnInterrupt)
    {
        if (btnPin != NULL)
            delete btnPin;
        btnPinI = new InterruptIn(btnPinName);
        lastChanged = millis();
        btnPinI->rise(callback(this, &DebouncedBtn::interruptCallBack));
        btnPinI->fall(callback(this, &DebouncedBtn::interruptCallBack));
        btnPinI->enable_irq(); // might enable for the entire port? not clear...
    }
}

int DebouncedBtn::read()
{
    if (mode == DebounceMode::Simple)
    {
        int currTime = millis();
        int newState = btnPin->read();
        if (newState != lastState) // something just changed, inconclusive
        {
            lastState = newState;
            lastTime = currTime;
            return btnState;
        }
        else // steady from last time
        {
            if (currTime - lastTime >= debounceTime) // steady for long enough
            {
                btnState = newState;
                return btnState;
            } // not steady for long enough, still waiting
            else
                return btnState;
        }
    }
    if (mode == DebounceMode::ListenOnInterrupt)
    {
        if (millis() - lastChanged >=debounceTime)
            return btnPinI->read();
        else
            return 0;
    }
    return 0;
}

void DebouncedBtn::setDebounceTime(int time_ms)
{
    debounceTime = time_ms;
}

void DebouncedBtn::interruptCallBack()
{
    lastChanged = millis();
}