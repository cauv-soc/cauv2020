#ifndef SERIAL_DECODER_H
#define SERIAL_DECODER_H

#include "mbed.h"
#include "instruction.h"
#include "containers.h"
#include "serialHandler.h"
using namespace mbed;

class DataPacker{
    private:
        // velocity is limited to + or - MAX_VELOCITY
        float pid_scale;

    public:
        DataPacker();
        void decodeTargetRotation(InstructionContainer, Rotation&);
        void decodeTargetVelocity(InstructionContainer, Velocity&);
        void decodePID(InstructionContainer, PID&);
        void decodeMBEDLights(InstructionContainer, MBEDLights&);
        void decodeHeadlights(InstructionContainer, Headlights&);

        void encodeCurrentRotation(Rotation, char*);
};
#endif