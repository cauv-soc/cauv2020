#ifndef INSTRUCTION_H
#define INSTRUCTION_H

#define MAX_DATA_FIELD_LENGTH 6

enum class Instruction
{
END_MESSAGE,
ECHO,
SET_SPEED_RAW,
SET_IMU_REPORT,
SET_SPEED_REPORT,
CONTROLLER_INPUT,
SET_TARGET_VELOCITY,
SET_TARGET_ROTATION,
SET_PID_VALUE,
SET_MBED_LIGHTS,
SET_HEAD_LIGHTS
};

int instructionSizeMapper(Instruction instruction);

#endif