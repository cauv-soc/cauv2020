#include "instruction.h"

int instructionSizeMapper(Instruction instruction)
{
    switch(instruction)
    {
        case Instruction::END_MESSAGE:
            return 0;
        case Instruction::ECHO:
            return 1;
        case Instruction::SET_SPEED_RAW:
            return 6;
        case Instruction::SET_IMU_REPORT:
            return 1;
        case Instruction::SET_SPEED_REPORT:
            return 1;
        case Instruction::CONTROLLER_INPUT:
            return 5;
        case Instruction::SET_TARGET_VELOCITY:
            return 4;
        case Instruction::SET_TARGET_ROTATION:
            return 4;
        case Instruction::SET_PID_VALUE:
            return 6;
        case Instruction::SET_MBED_LIGHTS:
            return 3;
        case Instruction::SET_HEAD_LIGHTS:
            return 4;
    }
}
