#ifndef SERIALHANDLER_H
#define SERIALHANDLER_H

#include "mbed.h"
#include "instruction.h"
#include "UARTSerial.h"
#include "reply.h"
using namespace mbed;

class InstructionContainer 
{
public:
    Instruction instruction;
    char data_field[MAX_DATA_FIELD_LENGTH];
};

enum class PortScanResult {STILL_WAITING, FRAME_READY, GENERIC_ERROR, DATA_LENGTH_MISMATCH, MISSING_END_MESSAGE};

class SerialHandler
{
public:
    SerialHandler(UARTSerial* _serialPort);
    // fucntion used for getting instructions
    PortScanResult scanPort(InstructionContainer* newInstruction);
    
    // yet to be tested
    void reply(Reply replyMsg, char* msg);

    void echo(); // just go into echo mode indefinitely
private:    
    UARTSerial* serialPort;
    Instruction currInstruction;
    char dataBuf[MAX_DATA_FIELD_LENGTH + 1];
    int currDataInd;
    int expectedLen;

    enum State {InstructionScanning, DataScanning};
    State currState;

    // some of the debug messages
    void print_frame_ready();
    void print_error(const char* errorMsg);
    void print_msg(const char* msg);
    void print_incoming(char incoming);
};
#endif