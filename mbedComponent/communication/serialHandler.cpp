#include "serialHandler.h"
#include "instruction.h"
#include "platform/mbed_thread.h"
#include "reply.h"
#include "myDebug.h"

#define SH_DEBUG true
#define DEBUG_DELAY 0 // ms

SerialHandler::SerialHandler(UARTSerial* _serialPort)
{
    serialPort = _serialPort;
    currInstruction = Instruction::END_MESSAGE;
    currDataInd = 0;
    currState = State::InstructionScanning;
}

PortScanResult SerialHandler::scanPort(InstructionContainer *newInstruction)
{
    // print_msg("enter scan port");
    if (currState == State::InstructionScanning)
    {
        // print_msg("scanning for instruction");
        char c = 0;
        while (true)
        {
            // check if there is something to read
            int len = serialPort->read(&c, 1);
            if (len != 1)
            {
                // print_msg("still waiting");
                return PortScanResult::STILL_WAITING;
            }

            print_incoming(c);
            // check if an instruction is read
            if (c > 128)
            {
                currInstruction = (Instruction) (c - 128);
                expectedLen = instructionSizeMapper(currInstruction);
                currDataInd = 0;
                currState = State::DataScanning;
                print_msg("Instrunction found.");
                break;
            }
        }
    }

    if (currState == State::DataScanning)
    {
        // print_msg("scanning for data");

        char c = 0;
        while (true)
        {
            // check if there is something to read
            int len = serialPort->read(&c, 1);
            if (len != 1)
            {
                // print_msg("still waiting");
                return PortScanResult::STILL_WAITING;
            }
            print_incoming(c);

            // if it is a data field
            if (c < 128)
            {
                dataBuf[currDataInd] = c;
                currDataInd++;
            }
            // if it is END_MESSAGE
            if (c == 128)
            {
                currState = State::InstructionScanning;
                if (currDataInd == expectedLen)// i.e. the message is correct
                {
                    // if the thing is empty, make an object
                    if (newInstruction == NULL)
                        newInstruction = new InstructionContainer;
                    // essentially copy everything over
                    newInstruction->instruction = currInstruction;
                    for(int i = 0; i < currDataInd; i++)
                        newInstruction->data_field[i] = dataBuf[i];
                    print_frame_ready();
                    return PortScanResult::FRAME_READY;
                }
                else
                {
                    print_error("data length mismatch");
                    return PortScanResult::DATA_LENGTH_MISMATCH;
                }
            }
            
            // if an instruction is hit before END_MESSAGE
            // this is a bit more troublesome and it is not so properly handled yet
            if (c > 128)
            {
                currState = State::InstructionScanning;
                print_error("missing end message");
                return PortScanResult::MISSING_END_MESSAGE;
            }
            
            // if the thing is over-flowing
            if (currDataInd > expectedLen)
            {
                currState = State::InstructionScanning;
                print_error("data length mismatch due to over flow");
                return PortScanResult::DATA_LENGTH_MISMATCH;
            }
        }
    }
    print_error("generic error");
    return PortScanResult::GENERIC_ERROR;
}

// the generic code for sending a reply
// not tested yet
void SerialHandler::reply(Reply replyMsg, char* msg)
{
    char startingMsg = 128 + (int)replyMsg;
    char endingMsg = 128;
    serialPort->write(&startingMsg, 1);
    serialPort->write(msg, replySizeMapper(replyMsg));
    serialPort->write(&endingMsg, 1);
}


// debug related functions
void SerialHandler::print_frame_ready()
{
    if (!SH_DEBUG)
        return;
    
    myDebug("=================================\r\n");
    myDebug("frameReady\r\n");
    myDebug("currInstruction:%d\r\n", currInstruction);
    myDebug("data:");

    for(int i = 0; i < currDataInd; i++)
        myDebug(" %d", dataBuf[i]);
    myDebug("\r\n=================================\r\n");
    thread_sleep_for(DEBUG_DELAY);
}

void SerialHandler::print_error(const char* errorMsg)
{
    if (!SH_DEBUG)
        return;

    myDebug("=================================\r\n");
    myDebug("%s\r\n", errorMsg);

    myDebug("currInstruction:");
    myDebug("%d\r\n", currInstruction);

    myDebug("data:");
    for(int i = 0; i < currDataInd; i++)
        myDebug(" %d", dataBuf[i]);
    myDebug("\r\n=================================\r\n");

    thread_sleep_for(DEBUG_DELAY);
}

void SerialHandler::print_msg(const char* msg)
{
    if (!SH_DEBUG)
        return;   
    myDebug("%s\r\n", msg);
    thread_sleep_for(DEBUG_DELAY);
}

void SerialHandler::print_incoming(char incoming)
{
    if (!SH_DEBUG)
        return;   
    myDebug("incoming %d\r\n", incoming);
    thread_sleep_for(DEBUG_DELAY);
}

void SerialHandler::echo()
{
    if (!SH_DEBUG)
        return;
    
    char c;
    while (true)
    {
        if (serialPort->read(&c, 1) == 1)
        {
            serialPort->write(&c, 1);
            myDebug("%d", c);
        }
    }
}