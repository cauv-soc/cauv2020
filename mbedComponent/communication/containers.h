#ifndef CONTAINERS_H
#define CONTAINERS_H

#include "mbed.h"
using namespace mbed;

struct Rotation
{
    float roll;
    float pitch;
    float yaw;
};

struct Velocity
{
    float vx;
    float vy;
    float vz;
};

struct TargetMotion
{
    Velocity target_velocity;
    Rotation target_rotation;
};

struct PID
{
    float p;
    float i;
    float d;
};

struct MBEDLights
{
    int light_1_brightness;
    int light_2_brightness;
    int light_3_brightness;
};

struct Headlights
{
    int light_1_brightness;
    int light_2_brightness;
    int light_3_brightness;
    int light_4_brightness;
};
#endif




