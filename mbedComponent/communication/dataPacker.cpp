#include "dataPacker.h"
#include "serialHandler.h"
#include "mbed.h"
#include "math.h"
#include "instruction.h"
#include "containers.h"
#include "myDebug.h"


float map_range(float x, float in_low, float in_high, float out_low, float out_high) 
{
    return ((x - in_low) / (in_high - in_low)) * (out_high - out_low) + out_low;
}

DataPacker::DataPacker()
{
    // More constructor stuff probably to come
    pid_scale = 1.0/16.0;
}

// Decoding functions

void DataPacker::decodeTargetVelocity(InstructionContainer i, Velocity &result) 
{
    if (i.instruction == Instruction::SET_TARGET_VELOCITY) 
    {
        result.vx = float(i.data_field[1]) / 16.0;
        result.vy = float(i.data_field[2]) / 16.0;
        result.vz = float(i.data_field[3]) / 16.0;

        // read the signs
        if ((i.data_field[0] & 1) == 1) {
            result.vx = -result.vx;
        }
        if ((i.data_field[0] & 2) == 2) {
            result.vy = -result.vy;
        }
        if ((i.data_field[0] & 4) == 4) {
            result.vz = -result.vz;
        }
    }
}

void DataPacker::decodeTargetRotation(InstructionContainer i, Rotation &result) 
{
    if (i.instruction == Instruction::SET_TARGET_ROTATION) 
    { 
        // resolution is limited to 2 degrees since it is unlikely the robot will be abel to hold position to greater accuracy
        result.yaw = i.data_field[1] * 2 * (((i.data_field[0] & 1) == 1) ? -1 : 1);
        result.pitch = i.data_field[2] * (((i.data_field[0] & 2) == 2) ? -1 : 1);
        result.roll = i.data_field[3] * 2 * (((i.data_field[0] & 4) == 4) ? -1 : 1);
    }
}

void DataPacker::decodePID(InstructionContainer i, PID &result)
{
    if (i.instruction == Instruction::SET_PID_VALUE) 
    {
        uint16_t P = ((i.data_field[0] & 63) << 7) + i.data_field[1];
        uint16_t I = ((i.data_field[1] & 63) << 7) + i.data_field[3];
        uint16_t D = ((i.data_field[2] & 63) << 7) + i.data_field[5];

        // resolution 0.1
        result.p =  P * pid_scale * (((i.data_field[0] & 64) == 64) ? -1 : 1);
        result.i =  I * pid_scale * (((i.data_field[2] & 64) == 64) ? -1 : 1);
        result.d =  D * pid_scale * (((i.data_field[4] & 64) == 64) ? -1 : 1);
    }
}

void DataPacker::decodeMBEDLights(InstructionContainer i, MBEDLights &result)
{
    if (i.instruction == Instruction::SET_MBED_LIGHTS) 
    {
        result.light_1_brightness = i.data_field[0];
        result.light_2_brightness = i.data_field[1];
        result.light_3_brightness = i.data_field[2];
    }
}

void DataPacker::decodeHeadlights(InstructionContainer i, Headlights &result)
{
    if (i.instruction == Instruction::SET_HEAD_LIGHTS) 
    {
        result.light_1_brightness = i.data_field[0];
        result.light_2_brightness = i.data_field[1];
        result.light_3_brightness = i.data_field[2];
        result.light_4_brightness = i.data_field[3];
    }
}

// Encoding functions

void DataPacker::encodeCurrentRotation(Rotation imu_rot, char* data) 
{
    // floating point maths may need to be optimised further
    uint16_t yaw_int = abs(int(round(imu_rot.yaw * 32.0))) + (imu_rot.yaw < 0 ? 8192 : 0);
    uint16_t pitch_int = abs(int(round(imu_rot.pitch * 64.0)))  + (imu_rot.pitch < 0 ? 8192 : 0);
    uint16_t roll_int = abs(int(round(imu_rot.roll * 32.0)))  + (imu_rot.roll < 0 ? 8192 : 0);

    char d[6] = {uint8_t(yaw_int >> 7), 
            uint8_t(yaw_int & 127), 
            uint8_t(pitch_int >> 7), 
            uint8_t(pitch_int & 127), 
            uint8_t(roll_int >> 7), 
            uint8_t(roll_int & 127)};

    strcpy(data, d);
}

