#include "reply.h"

int replySizeMapper(Reply instruction)
{
    switch(instruction)
    {
        case Reply::END_MESSAGE:
            return 0;
        case Reply::ECHO:
            return 1;
        case Reply::IMU_ROTATION_READING:
            return 6;
        case Reply::TARGET_ORIENTATION:
            return 4;
        case Reply::IMU_ANGULAR_SPEED:
            return 4;
        case Reply::IMU_LINEAR_ACC:
            return 4;
        case Reply::RAW_SPEED:
            return 8;
        case Reply::INTENDED_SPEED:
            return 8;
        case Reply::BTN_STATUS:
            return 1;
    }
}
