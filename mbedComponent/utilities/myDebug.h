#ifndef MYDEBUG_H
#define MYDEBUG_H

#include "cstdarg"
#include "mbed.h"

#define DEBUG_COM_TX USBTX
#define DEBUG_COM_RX USBRX

void myDebug(const char * format, ... );

#endif