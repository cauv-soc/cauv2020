#include "globalTimer.h"

Timer* globalTimer = NULL;

// implement some thing like arduino style millis
int millis()
{
    if (globalTimer == NULL)
    {
        globalTimer = new Timer();
        globalTimer->start();
    }
    return globalTimer->read_ms();
}

uint64_t microseconds()
{
    if (globalTimer == NULL)
    {
        globalTimer = new Timer();
        globalTimer->start();
    }
    return globalTimer->read_high_resolution_us();
}

float seconds()
{
    if (globalTimer == NULL)
    {
        globalTimer = new Timer();
        globalTimer->start();
    }
    return globalTimer->read();
}