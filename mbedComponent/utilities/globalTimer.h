#ifndef GLOBAL_TIMER_H
#define GLOBAL_TIMER_H
#include "mbed.h"

extern Timer* globalTimer;

// implement some thing like arduino style millis
int millis();

// caution, not tested yet
uint64_t microseconds();

float seconds();

#endif